import { AnalysisWorkerWrapper, createWorker, Paper } from "yoastseo";

// `url` needs to be the full URL to the script for the browser to know where to load the worker script from.
// This should be the script created by the previous code-snippet.
const url = "/js/worker.js"
console.log('YOAST is HERE!');


const worker = new AnalysisWorkerWrapper( createWorker( url ) );
worker.initialize( {
    locale: "ar_EG",
    contentAnalysisActive: true,
    keywordAnalysisActive: true,
    logLevel: "ERROR",
} );
export function analyzeContent(text,attributes)
{
    const paper = new Paper( text, attributes );
    return worker.analyze( paper );
}

