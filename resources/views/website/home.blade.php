@extends('website.layouts.main')

@section('content')
@include('website.layouts.slider')
    <!-- Post -->
<section class="bg0 p-t-40">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8">
                <div class="p-b-20">
                    @if ($importantArticles->count())
                        @include('website.components.nav-tab',['link'=>route('article.mostImportant'),'index'=>0,'title'=>'اهم الاحداث','allArticles'=>$importantArticles,'categories'=>$importantCategories])
                    @endif

                    @foreach ($homeCategories as $category)
                        @if ($category->articles()->count())
                            @include('website.components.nav-tab',['link'=>route('section.single',$category->name),'index'=>$loop->index+1,'title'=>$category->name,'allArticles'=>$category->articles()->limit(4)->get(),'categories'=>$category->children])
                        @endif
                    @endforeach
                </div>
            </div>

            @include('website.layouts.leftbar')
        </div>
    </div>
</section>

<!-- Banner -->
<div class="container-fluid">
    <div class="flex-c-c">
        <a href="#">
            <img class="max-w-full" src="{{asset('site/images/banner-01.jpg')}}" alt="IMG">
        </a>
    </div>
</div>

<!-- Latest -->
<section class="bg0 p-t-60 p-b-35">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 p-b-20">
                {{-- @include('website.components.nav-tab-video') --}}
            </div>

            <div class="col-md-10 col-lg-4">
                <div class="p-l-10 p-rl-0-sr991 p-b-20">

                    <!-- Subscribe -->
                    <div class="bg10 p-rl-35 p-t-28 p-b-35 m-b-55">
                        <h5 class="f1-m-5 cl0 p-b-10">
                            اشترك معنا
                        </h5>

                        <p class="f1-s-1 cl0 p-b-25">
                            اشترك الاّن لتصلك مفالاتنا الجديدة دائما
                        </p>

                        <form class="size-a-9 pos-relative" action="{{route('subscribe')}}" method="POST">
                            @csrf
                            {{-- @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                            @endif --}}
                            <input class="s-full f1-m-6 cl6 plh9 p-r-20 p-l-55" type="email" name="email" placeholder="البريد الالكترونى">

                            <button class="size-a-10 flex-c-c ab-t-l fs-16 cl9 hov-cl10 trans-03">
                                <i class="fa fa-arrow-left"></i>
                            </button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')

@endsection
