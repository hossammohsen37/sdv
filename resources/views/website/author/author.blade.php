@extends('website.layouts.main')
@section('content')
<!-- Breadcrumb -->
<div class="container-fluid">
    <div class="headline bg0 flex-wr-sb-c p-rl-20 p-tb-8">
        <div class="f2-s-1 p-r-30 m-tb-6" dir="ltr">
            <a href="#" class="breadcrumb-item f1-s-3 cl9">
                الرئسيه
            </a>

            <span class="breadcrumb-item f1-s-3 cl9">
                عن الكاتب
            </span>
        </div>
    </div>
</div>

<!-- Page heading -->
<div class="container-fluid p-t-4 p-b-35">
    <div class="d-flex">
        <img src="{{$author->profile_pic}}" class="authour_img" alt="">
        <h2 class="f1-l-1 cl2 align-self-center mr-3">
           {{$author->name}}
        </h2>
    </div>
</div>

<!-- Content -->
<section class="bg0 p-b-110">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-8 p-b-30">
                <div class="p-r-10 p-r-0-sr991">
                    <p class="f1-s-11 cl6 p-b-25">
                        {{$author->about}}
                    </p>
                </div>
            </div>

            <!-- Sidebar -->
            <div class="col-md-5 col-lg-4 p-b-30">
                <div class="p-l-10 p-rl-0-sr991 p-t-5">
                    <!-- Popular Posts -->
                    <div>
                        <div class="how2 how2-cl4 flex-s-c">
                            <h3 class="f1-m-2 cl3 tab01-title">
                                مقالات الكاتب
                            </h3>
                        </div>

                        <ul class="p-t-35">
                            @foreach ($articles as $article)
                                <li class="flex-wr-sb-s p-b-30">
                                    <a href="{{route('article.single',$article->slug)}}" class="size-w-10 wrap-pic-w hov1 trans-03">
                                        <img src="{{$article->cover_photo}}" alt="IMG">
                                    </a>

                                    <div class="size-w-11">
                                        <h6 class="p-b-4">
                                            <a href="{{route('article.single',$article->slug)}}" class="f1-s-5 cl3 hov-cl10 trans-03">
                                               {{$artricle->title}}
                                            </a>
                                        </h6>

                                        <span class="cl8 txt-center p-b-24">
                                            <a href="{{route('section.single',$childCategory->name)}}" class="f1-s-6 cl8 hov-cl10 trans-03">
                                                {{$article->category->name}}
                                            </a>

                                            <span class="f1-s-3 m-rl-3">
                                                -
                                            </span>

                                            <span class="f1-s-3">
                                                {{$author->created_at->format('M d')}}
                                            </span>
                                        </span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('javascript')
@endsection
