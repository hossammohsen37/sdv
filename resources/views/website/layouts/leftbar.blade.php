<div class="col-md-10 col-lg-4">
    <div class="p-l-10 p-rl-0-sr991 p-b-20">
        <!--Trends  -->
        @if ($trends->count())
        <div>
            <div class="how2 how2-cl4 flex-s-c">
                <h3 class="f1-m-2 cl3 tab01-title">
                    اهم الترندات
                </h3>
            </div>

            <div class="trendsSlider">
                @foreach ($trends as $trend)
                    <div class="item">
                        <div class="flex-c-s p-t-8 h-100">
                            <a href="{{$trend->getUrl()}}" class="d-flex align-items-center justify-content-center h-100"
                                style="background-image: url({{$trend->cover_image_url}})">
                                {{-- <img class="max-w-full" src="" alt="IMG"> --}}
                                <h5 class="f1-m-5 cl0 p-b-10 mb-3">
                                    {{$trend->name}}
                                </h5>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @endif

        {{-- {{route('article.single',$new->slug)}} --}}
        <!--News  -->
        @if ($news->count())
            <div>
                <div class="how2 how2-cl4 flex-s-c">
                    <h3 class="f1-m-2 cl3 tab01-title">
                        أخبار
                    </h3>
                </div>
                <ul class="p-t-20" id="newsLeftBar_slider">

                        @foreach ($news as $new)
                        <div class="swiper-wrapper">
                            <li class="flex-wr-sb-s p-b-20 swiper-slide">
                                <a href="{{route('article.single',$new->slug)}}" class="size-w-4 wrap-pic-w hov1 trans-03">
                                    <img src="{{$new->cover_photo}}" alt="IMG">
                                </a>

                                <div class="size-w-5">
                                    <h6 class="p-b-5">
                                        <a href="{{route('article.single',$new->slug)}}" class="size-w-3 f1-s-7 cl3 hov-cl10 trans-03">
                                            {{$new->title}}
                                        </a>
                                    </h6>

                                    <span class="f1-s-3 cl6">
                                        {{$new->created_at->format('M d')}}
                                    </span>
                                </div>
                            </li>
                        </div>
                        @endforeach
                </ul>
            </div>
        @endif

        <!--Author -->

        @if ($authorArticles->count())
            <div class="p-b-55">
                <div class="how2 how2-cl4 flex-s-c m-b-30">
                    <h3 class="f1-m-2 cl3 tab01-title">
                        مؤلفون
                    </h3>
                </div>
                <div class="flex-wr-s-s m-rl--5">
                @foreach ($authorArticles as $article)
                    <li class="flex-wr-sb-s p-b-20">
                    <a href="{{route('article.single',$article->slug)}}" class="size-w-4 wrap-pic-w hov1 trans-03">
                            <img  class="auth_pic" src="{{$article->author->profile_pic}}" alt="IMG">
                        </a>

                        <div class="size-w-5">
                            <h6 class="p-b-5">
                                <a href="{{route('article.single',$article->slug)}}" class="size-w-3 f1-s-7 cl3 hov-cl10 trans-03">
                                    {{$article->author->name}}
                                </a>
                            </h6>

                            <span class="f1-s-3 cl6">
                                {{$article->author->about}}
                            </span>
                        </div>
                    </li>
                @endforeach
             </div>
            </div>
        @endif



        <!-- Video -->
        @if ($infographic)
        <div class="p-b-55">
            <div class="how2 how2-cl4 flex-s-c m-b-35">
                <h3 class="f1-m-2 cl3 tab01-title">
                    Infographic
                </h3>
            </div>
            <div>
                <div class="wrap-pic-w pos-relative">
                    <img src="{{$infographic->cover_photo}}" alt="IMG">

                    <!-- <button class="s-full ab-t-l flex-c-c fs-32 cl0 hov-cl10 trans-03" data-toggle="modal" data-target="#modal-video-01">
                        <span class="fab fa-youtube"></span>
                    </button> -->
                </div>

                <div class="p-tb-16 p-rl-25 bg3">
                    <h5 class="p-b-5">
                        <a href="{{route('article.single',$infographic->slug)}}" class="f1-m-3 cl0 hov-cl10 trans-03">
                            {{$infographic->title}}
                        </a>
                    </h5>

                    <span class="cl15">
                        {{-- <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                            by John Alvarado
                        </a>

                        <span class="f1-s-3 m-rl-3">
                            -
                        </span> --}}

                        <span class="f1-s-3">
                            {{$infographic->created_at->format('M d')}}
                        </span>
                    </span>
                </div>
            </div>
        </div>
        @endif

        <div class="row">
            <div class="col-12">
                <iframe
                    src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsdvmag%2F&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"
                    height="500"
                    style="border:none;width: 90%;"
                    scrolling="yes" frameborder="0" allowTransparency="true" allow="encrypted-media">
                </iframe>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a class="twitter-timeline" data-height="500" data-theme="light"
                    href="https://twitter.com/Omar_abo_ghader?ref_src=twsrc%5Etfw">
                    Tweets by Omar_abo_ghader
                </a>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
        </div>


    </div>
</div>
