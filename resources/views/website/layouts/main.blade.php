<!DOCTYPE html>
<html lang="ar" dir="rtl">
<head>
	{{-- <title>SDV</title> --}}
	@seoTags()
	@include('website.layouts.head')
</head>
<body class="animsition">	 
	<header>
		@include('website.layouts.header')
	</header>
        @include('website.layouts.sidebar')		
        
        @yield('content')
	<footer>
		@include('website.layouts.footer')
	</footer>
    @include('website.layouts.scripts')
    @yield('javascript')
</body>
</html>