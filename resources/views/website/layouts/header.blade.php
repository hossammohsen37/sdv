<div class="container-menu-desktop">

    <!-- Header Mobile -->
    <div class="wrap-header-mobile">
        <!-- Logo moblie -->
        <div class="logo-mobile">
            <a href="/"><img src="{{asset('site/images/icons/logo-01.png')}}" alt="site-logo"></a>
        </div>

        <!-- Button show menu -->
        <div class="btn-show-menu-mobile hamburger hamburger--squeeze m-r--8">
            <span class="hamburger-box">
                <span class="hamburger-inner"></span>
            </span>
        </div>
    </div>

    <!-- Menu Mobile -->
    <div class="menu-mobile">

        <ul class="main-menu-m">
            <li>
                <a href="{{route('home')}}">الرئيسية</a>
            </li>
            @foreach ($categories as $category)
                @if ($category->children()->count())
                    <li>
                        <a class="has-sub-menu">{{$category->name}}</a>
                        <ul class="sub-menu-m">
                            @foreach ($category->children as $childCategory)
                                <li><a href="{{route('section.single',$childCategory->name)}}">{{$childCategory->name}}</a></li>
                            @endforeach
                        </ul>
                        <span class="arrow-main-menu-m">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </span>
                    </li>
                @else
                    <li>
                        <a href="{{route('section.single',$category->name)}}">{{$category->name}}</a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>

    <!--  -->
    <div class="wrap-logo container">
        <!-- Logo desktop -->
        <div class="logo">
            <a href="/"><img src="{{asset('site/images/icons/logo-01.png')}}" alt="LOGO"></a>
        </div>

        <!-- Banner -->
        <!-- <div class="banner-header">
            <a href="#"><img src="images/banner-01.jpg" alt="IMG"></a>
        </div> -->
    </div>

    <!--  -->
    <div class="wrap-main-nav">
        <div class="main-nav">
            <!-- Menu desktop -->
            <nav class="menu-desktop">
                <a class="logo-stick" href="{{route('home')}}">
                    <img src="{{asset('site/images/icons/logo-01.png')}}" alt="LOGO">
                </a>

                <ul class="main-menu">
                    <li>
                        <a href="{{route('home')}}">الرئيسية</a>
                    </li>
                    @foreach ($categories as $category)
                    @if ($category->children()->count())
                    <li>
                        <a class="has-sub-menu">{{$category->name}}</a>
                        <ul class="sub-menu">
                            @foreach ($category->children as $childCategory)
                                <li><a href="{{route('section.single',$childCategory->name)}}">{{$childCategory->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="{{route('section.single',$category->name)}}">{{$category->name}}</a>
                    </li>
                    @endif
                    @endforeach

                </ul>
            </nav>
        </div>
    </div>
</div>
