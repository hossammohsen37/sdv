<ul class="services__sideBar">
    <li class="service__title">
        <i class="fas fa-book-reader"></i>
        <p>الأكثر قراءه</p>
        <div class="service_overlay">
            @foreach ($maxArticle as $article)
                <a href="{{route('article.single',$article->slug)}}" class="mostReade d-flex">
                    <p>
                        <i class="fas fa-sort-amount-up"></i>
                        <span class="counter">{{$article->visits}}</span>
                    </p>
                    <h3>
                        {{$article->title}}
                    </h3>
                </a>
            @endforeach
            <a href="#" class="sdv-btn text-center">
                المزيد
            </a>
        </div>
    </li>
    <li class="service__title">
        <i class="fas fa-mosque"></i>
        <p>مواقيت الصلاه</p>
        <div class="service_overlay">
            <div class="prayingTime"></div>
        </div>
    </li>
    <li class="service__title">
        <i class="fas fa-search-dollar"></i>
        <p>أسعار العملات</p>
        <div class="service_overlay currency"></div>
    </li>
    <li class="service__title">
        <i class="fas fa-coins"></i>
        <p>أسعار الذهب</p>
        <div class="service_overlay">
            <div class="text-center" style="padding: 1.5rem 0;">
                <p>سعر الذهب اليوم</p>
                <h2 style="font-size: 1.7rem;">{{App\Models\Statics::getWithCurrentLang('gold_price')}} جنيه</h2>
                <span>عيار 21</span>
            </div>
        </div>
    </li>
    <li class="service__title">
        <i class="fas fa-smog"></i>
        <p>الطقس</p>
        <div class="service_overlay weather"></div>
    </li>
    <li class="service__title">
        <i class="fas fa-search"></i>
        <p>أبحث عن</p>
        <div class="service_overlay">
            <form class="searchingFor" method="GET" action="{{route('search')}}">
                <input class="bo-1-rad-3 bocl13 size-a-16 f1-s-13 cl5 plh6 p-rl-18 m-b-10" type="text" name="q" placeholder="أبحث عن">
                <button class="sdv-btn text-center">
                    <i class="fas fa-search"></i>
                    بحث
                </button>
            </form>
        </div>
    </li>
</ul>

