<!-- Headline -->
@if ($trends->count())
<div class="container-fluid">
    <div class="bg0 flex-wr-sb-c p-rl-20 p-tb-8">
        <div class="f2-s-1 p-r-30 size-w-0 m-tb-6 flex-wr-s-c">
            <span class="text-uppercase cl2 p-l-8">
                الترندات اليوم:
            </span>

            <span class="dis-inline-block cl6 slide100-txt pos-relative size-w-0" data-in="fadeInDown" data-out="fadeOutDown">
                @foreach ($trends as $trend)
                <span class="dis-inline-block slide100-txt-item animated visible-false">
                    {{$trend->name}}
                </span>
                @endforeach
            </span>
        </div>
    </div>
</div>
@endif
<section class="bg0">
    <div class="container-fluid">
        <div class="row m-rl--1">
            <div class="col-md-8 p-rl-1 p-b-2">
                <div class="mainSlider">
                    @foreach ($sliders as $slider)
                        <div class="item">
                            <div class="bg-img1 size-a-3 how1 pos-relative" style="background-image: url({{$slider->cover_photo}});">
                                <a href="{{route('article.single',$slider->slug)}}" class="dis-block how1-child1 trans-03"></a> <!--blog-detail-01.html-->

                                <div class="flex-col-e-s s-full p-rl-25 p-tb-20">
                                    <a href="#" class="dis-block how1-child2 f1-s-2 cl0 bo-all-1 bocl0 hov-btn1 trans-03 p-rl-5 p-t-2">
                                        {{$slider->category->name}}
                                    </a>

                                    <h3 class="how1-child2 m-t-14 m-b-10">
                                        <a href="{{route('article.single',$slider->slug)}}" class="how-txt1 size-a-6 f1-l-1 cl0 hov-cl10 trans-03"> <!--blog-detail-01.html-->
                                            {{$slider->title}}
                                        </a>
                                    </h3>

                                    <span class="how1-child2">
                                        <span class="f1-s-4 cl11">
                                            {{$slider->author_name}}
                                        </span>

                                        <span class="f1-s-3 cl11 m-rl-3">
                                            -
                                        </span>

                                        <span class="f1-s-3 cl11">
                                            {{$slider->created_at->format('M d')}}
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="col-md-4">
                <div class="banner">
                    <img src="{{asset('site/images/banner-02.jpg')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
