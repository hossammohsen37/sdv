<div class="bg2 p-t-40 p-b-25">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 p-b-20">
                <div class="size-h-3 flex-s-c">
                    <a href="#">
                        <img style="width: 200px;" class="max-s-full" src="{{asset('site/images/icons/logo-02.png')}}" alt="LOGO">
                    </a>
                </div>

                <div>
                    <p class="f1-s-1 cl11 p-b-16 p-t-16">
                        {{App\Models\Statics::getWithCurrentLang('about')}}
                    <p class="f1-s-1 cl11 p-b-16">
                        للاستفسار كلمنا على <span dir="ltr">{{App\Models\Statics::get('phone')}}</span> <br>
                        او راسلنا على الميل <span>{{App\Models\Statics::getWithCurrentLang('email')}}</span>
                    </p>

                    <div class="p-t-15">
                        @if (App\Models\Statics::getWithCurrentLang('facebook'))
                        <a href="{{App\Models\Statics::getWithCurrentLang('facebook')}}" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
                            <span class="fab fa-facebook-f"></span>
                        </a>
                        @endif

                        @if (App\Models\Statics::get('twitter'))
                        <a href="{{App\Models\Statics::get('twitter')}}" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
                            <span class="fab fa-twitter"></span>
                        </a>
                        @endif

                        {{-- <a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
                            <span class="fab fa-pinterest-p"></span>
                        </a>

                        <a href="#" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
                            <span class="fab fa-vimeo-v"></span>
                        </a> --}}
                        @if (App\Models\Statics::getWithCurrentLang('youtube'))
                        <a href="{{App\Models\Statics::getWithCurrentLang('youtube')}}" class="fs-18 cl11 hov-cl10 trans-03 m-r-8">
                            <span class="fab fa-youtube"></span>
                        </a>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4 p-b-20">
                @if ($maxArticle->count())
                <div class="size-h-3 flex-s-c">
                    <h5 class="f1-m-7 cl0">
                        الاكثر قراءه
                    </h5>
                </div>

                <ul>
                    @foreach ($maxArticle as $article)
                        <li class="flex-wr-sb-s p-b-20">
                            <a href="{{route('article.single',$article->slug)}}" class="size-w-4 wrap-pic-w hov1 trans-03">
                                <img src="{{$article->cover_photo}}" alt="IMG">
                            </a>

                            <div class="size-w-5">
                                <h6 class="p-b-5">
                                    <a href="{{route('article.single',$article->slug)}}" class="f1-s-5 cl11 hov-cl10 trans-03">
                                        {{$article->title}}
                                    </a>
                                </h6>

                                <span class="f1-s-3 cl6">
                                    {{$article->created_at->format('M d')}}
                                </span>
                            </div>
                        </li>
                    @endforeach
                </ul>
                @endif

            </div>
            @if ($categories->count())
                <div class="col-sm-6 col-lg-4 p-b-20">
                    <div class="size-h-3 flex-s-c">
                        <h5 class="f1-m-7 cl0">
                            الاقسام
                        </h5>
                    </div>

                    <ul class="m-t--12">
                        @foreach ($categories as $category)
                        <li class="how-bor1 p-rl-5 p-tb-10">
                            <a href="{{route('section.single',$category->name)}}" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
                                {{$category->name}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>

<div class="bg11">
    <div class="container-fluid size-h-4 flex-c-c p-tb-15">
        <span class="f1-s-1 cl0 txt-center">
            <a href="#" class="f1-s-1 cl10 hov-link1">
                Copyright &copy;
                <script>document.write(new Date().getFullYear());</script>
                All rights reserved | This website is made with <i class="fa fa-heart" aria-hidden="true"></i> by H.O
            </a>
        </span>
    </div>
</div>
