<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="{{asset('site/images/icons/logo-01.png')}}"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('site\fonts\font-awesome-4.7.0\css\font-awesome.min.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{asset('site/vendor/bootstrap/css/bootstrap.min.css')}}">
    {{-- Social Sharing --}}
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />

	<link rel="stylesheet" type="text/css" href="{{asset('site/vendor/animate/animate.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('site/vendor/css-hamburgers/hamburgers.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('site/vendor/animsition/css/animsition.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('site/css/slider/slick-theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('site/css/slider/slick.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.js">

    <link rel="stylesheet" type="text/css" href="{{asset('site/fonts/iconic/css/material-design-iconic-font.min.css')}}">

	<link rel="stylesheet" type="text/css" href="{{asset('site/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('site/css/main.css')}}">
