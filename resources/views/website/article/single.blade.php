@extends('website.layouts.main')
@section('content')
    <!-- Breadcrumb -->
	<div class="container-fluid">
		<div class="headline bg0 flex-wr-sb-c p-rl-20 p-tb-8">
			<div class="f2-s-1 p-r-30 m-tb-6" dir="ltr">
				<a href="/" class="breadcrumb-item f1-s-3 cl9">
					الرئسيه
				</a>

				{{-- <a href="" class="breadcrumb-item f1-s-3 cl9">
					المقالات
				</a> --}}

				<span class="breadcrumb-item f1-s-3 cl9">
					{{$article->title}}
				</span>
			</div>
		</div>
	</div>

	<!-- Content -->
	<section class="bg0 p-b-70 p-t-5">
		<!-- Title -->
		<div class="bg-img1 size-a-18 how-overlay1" style="background-image: url({{$article->cover_photo}});">
			<div class="container-fluid h-full flex-col-e-c p-b-58">
				<a href="{{route('section.single',$article->category->slug)}}" class="f1-s-10 cl0 hov-cl10 trans-03 text-uppercase txt-center m-b-33">
					{{$article->category->name}}
				</a>

				<h1 class="f1-l-5 cl0 p-b-16 txt-center respon2">
					{{$article->title}}
				</h1>

				<div class="flex-wr-c-s">
					<span class="f1-s-3 cl8 m-rl-7 txt-center">
						@if ($article->display_author_name)
						<a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
							بقلم {{$article->display_author_name}}
						</a>
						@endif

						<span class="m-rl-3">-</span>

						<span>
							{{$article->created_at->format('M d')}}
						</span>
					</span>

					<span class="f1-s-3 cl8 m-rl-7 txt-center">
						{{$article->visits}} مشاهدة
                    </span>

                </div>
				<div class="flex-wr-c-s">
					<span class="f1-s-3 cl8 m-rl-7 txt-center">
						تستغرق {{$article->reading_time}} دقيقة
					</span>
                </div>

			</div>
		</div>

		<!-- Detail -->
		<div class="container-fluid p-t-82">
			<div class="row justify-content-center">
				<div class="col-md-10 col-lg-8 p-b-100">
					<div class="p-r-10 p-r-0-sr991">
						<!-- Blog Detail -->
						<div class="p-b-70">
							<div class="f1-s-11 cl6 p-b-25" class="__articleContent">
								{!!$article->content!!}
							</div>


							<div class="flex-s-s p-t-12 p-b-15">
								<span class="f1-s-12 cl5 m-r-8">
									بقلم:
								</span>
								@if ($article->display_author_name)
								<div class="flex-wr-s-s size-w-0">
									<a href="#" class="f1-s-12 cl8 hov-link1 m-r-15">
										{{$article->display_author_name}}
									</a>
								</div>
								@endif
							</div>

							<!-- Share -->
							<div class="flex-s-s align-items-center">
								<span class="f1-s-12 cl5 p-t-1 m-r-15">
									مشاركة:
								</span>

                                <div id="share"></div>
								{{-- <div class="flex-wr-s-s size-w-0">
									<a href="#" class="dis-block f1-s-13 cl0 bg-facebook borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-facebook-f m-r-7"></i>
										Facebook
									</a>

									<a href="#" class="dis-block f1-s-13 cl0 bg-twitter borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-twitter m-r-7"></i>
										Twitter
									</a>

									<a href="#" class="dis-block f1-s-13 cl0 bg-google borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-google-plus-g m-r-7"></i>
										Google+
									</a>

									<a href="#" class="dis-block f1-s-13 cl0 bg-pinterest borad-3 p-tb-4 p-rl-18 hov-btn1 m-r-3 m-b-3 trans-03">
										<i class="fab fa-pinterest-p m-r-7"></i>
										Pinterest
									</a>
								</div> --}}
                            </div>

                            <!-- Tag -->
							<div class="flex-s-s p-t-12 p-b-15 align-items-center">
								<span class="cl5 m-r-8">
									{{-- Tags: --}}
								</span>

								<div class="flex-wr-s-s size-w-0">
									@foreach ($article->tags as $tag)
									<a href="{{route('article.tag',['tag'=>$tag->name])}}" class="flex-c-c size-h-2 bo-1-rad-20 bocl12 f1-s-1 cl8 hov-btn2 trans-03 p-rl-20 p-tb-5 m-all-5">
										{{$tag->name}}
									</a>
									@endforeach
								</div>
							</div>
						</div>
                    </div>
					{{-- @if ($article->author)
                    <div class="row m-b-45">
                        <div class="col-12">
                            <div class="card bg12  p-t-20 p-b-20 p-r-15 p-l-15" >
                                <div class="how2 how2-cl4 flex-s-c m-b-30">
                                    <h3 class="f1-m-2 cl3 tab01-title">
                                        عن الكاتب
                                    </h3>
                                </div>
                                <li class="flex-wr-sb-s">
                                    <a href="#" class="size-w-4 wrap-pic-w hov1 trans-03">
                                        <img  class="auth_pic" src="{{$article->author->profile_pic}}" alt="{{$article->author->name}}">
                                    </a>

                                    <div class="size-w-5 align-self-center">
                                        <h6 class="p-b-5">
                                            <a href="#" class="size-w-3 f1-s-7 cl3 hov-cl10 trans-03">
                                               {{$article->author->name}}
                                            </a>
                                        </h6>

                                        <span class="f1-s-3 cl6">
                                            {{$article->author->about}}
                                        </span>
                                    </div>
                                </li>
                            </div>
                        </div>
                    </div>
					@endif --}}

                    <div class="row">
                        <div class="col-12">
                            <div class="how2 how2-cl4 flex-s-c m-b-30">
                                <h3 class="f1-m-2 cl3 tab01-title">
                                    مقالات مشابها
                                </h3>
                            </div>
                            <div class="relatedArticle_slider">
                                @foreach ($similiarArticles as $article)
                                <div class="item p-l-10 p-r-10">
                                    <a href="#" class="wrap-pic-w hov1 trans-03">
                                        <img src="{{$article->cover_photo}}" alt="IMG">
                                    </a>

                                    <div class="p-t-16">
                                        <h5 class="p-b-5">
                                            <a href="blog-detail.html" class="f1-m-3 cl2 hov-cl10 trans-03">
                                                {{$article->title}}
                                            </a>
                                        </h5>

                                        <span class="cl8">
                                            @if ($article->display_author_name)
                                            <div class="f1-s-4 cl8 hov-cl10 trans-03">
                                                بواسطة {{$article->author_name}}
                                            </div>
                                            @endif

                                            <span class="f1-s-3 m-rl-3">
                                                -
                                            </span>

                                            <span class="f1-s-3">
                                                {{$article->created_at->format('M d')}}
                                            </span>
                                        </span>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
				</div>

				<div class="col-md-10 col-lg-4 p-b-100">
					<div class="p-l-10 p-rl-0-sr991">
						<!-- Banner -->
						<div class="flex-c-s">
							<a href="#">
								<img class="max-w-full" src="{{asset('site/images/banner-02.jpg')}}" alt="IMG">
							</a>
						</div>
					</div>
				</div>
            </div>
		</div>
	</section>
@endsection
@section('javascript')

@endsection
