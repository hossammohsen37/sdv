@extends('website.layouts.main')
@section('content')
<div class="container-fluid">
    <div class="bg0 flex-wr-sb-c p-rl-20 p-tb-8">
        <div class="f2-s-1 p-r-30 m-tb-6" dir="ltr">
            <a href="index.html" class="breadcrumb-item f1-s-3 cl9">
                الرئيسيه
            </a>
            <span class="breadcrumb-item f1-s-3 cl9">
                 الاقسام
            </span>
        </div>
    </div>
</div>
<!-- Page heading -->
<div class="container-fluid p-t-4 p-b-40">
    <h2 class="f1-l-1 cl2">
        {{$title}}
    </h2>
</div>
<!-- Post -->
<section class="bg0 p-b-55">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 p-b-80">
                <div class="row">
                    @foreach ($articles as $article)
                        <div class="col-sm-6 p-r-25 p-r-15-sr991">
                            <div class="m-b-45">
                                <a href="{{route('article.single',$article->slug)}}" class="blog__img wrap-pic-w hov1 trans-03">
                                    <img src="{{$article->cover_photo}}" alt="IMG">
                                </a>
                                <div class="p-t-16">
                                    <h5 class="p-b-5">
                                        <a href="{{route('article.single',$article->slug)}}" class="f1-m-3 cl2 hov-cl10 trans-03">
                                            {{$article->title}}
                                        </a>
                                    </h5>
                                    <span class="cl8">
                                        <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                                            {{$article->author_name}}
                                        </a>
                                        <span class="f1-s-3 m-rl-3">
                                            -
                                        </span>
                                        <span class="f1-s-3">
                                            {{$article->created_at->format('M d')}}
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- Pagination -->
                {{ $articles->links('website.layouts.articlePagination') }}
            </div>
            @include('website.layouts.leftbar')
        </div>
    </div>
</section>

@endsection
@section('javascript')

@endsection
