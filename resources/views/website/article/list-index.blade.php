@extends('website.layouts.main')

@section('content')
<div class="container-fluid">
    <div class="bg0 flex-wr-sb-c p-rl-20 p-tb-8">
        <div class="f2-s-1 p-r-30 m-tb-6" dir="ltr">
            <a href="index.html" class="breadcrumb-item f1-s-3 cl9">
                الرئيسيه
            </a>

            <span class="breadcrumb-item f1-s-3 cl9">
                اسم الصفحة
            </span>
        </div>

    </div>
</div>
<!-- Page heading -->
<div class="container-fluid p-t-4 p-b-40">
    <h2 class="f1-l-1 cl2">
        أهم الاحداث
    </h2>
</div>
<!-- Post -->
<section class="bg0 p-b-55">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-8 p-b-80">
                <div class="p-r-10 p-r-0-sr991">
                    <div class="m-t--40 p-b-40">
                        <!-- Item post -->
                        <div class="flex-wr-sb-s p-t-40 p-b-15 how-bor2">
                            <a href="blog-detail.html"
                                class="size-w-8 wrap-pic-w hov1 trans-03 w-full-sr575 m-b-25">
                                <img src="{{asset('site/images/post-43.jpg')}}" alt="IMG">
                            </a>

                            <div class="size-w-9 w-full-sr575 m-b-25">
                                <h5 class="p-b-12">
                                    <a href="blog-detail.html" class="f1-l-1 cl2 hov-cl10 trans-03 respon2">
                                        هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم
                                    </a>
                                </h5>

                                <div class="cl8 p-b-18">
                                    <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                                        بوسطة محمد أحمد
                                    </a>

                                    <span class="f1-s-3 m-rl-3">
                                        -
                                    </span>

                                    <span class="f1-s-3">
                                        Feb 18
                                    </span>
                                </div>

                                <p class="f1-s-1 cl6 p-b-24">
                                    Duis eu felis id tortor congue consequat. Sed vitae vestibulum enim, et pharetra magna
                                </p>

                                <a href="blog-detail.html" class="f1-s-1 cl9 hov-cl10 trans-03">
                                    أقرأ المزيد
                                    <i class="m-l-2 fa fa-long-arrow-alt-left"></i>
                                </a>
                            </div>
                        </div>

                        <!-- Item post -->
                        <div class="flex-wr-sb-s p-t-40 p-b-15 how-bor2">
                            <a href="blog-detail.html"
                                class="size-w-8 wrap-pic-w hov1 trans-03 w-full-sr575 m-b-25">
                                <img src="{{asset('site/images/post-44.jpg')}}" alt="IMG">
                            </a>

                            <div class="size-w-9 w-full-sr575 m-b-25">
                                <h5 class="p-b-12">
                                    <a href="blog-detail.html" class="f1-l-1 cl2 hov-cl10 trans-03 respon2">
                                        Health lorem ipsum dolor sit amet consectetur
                                    </a>
                                </h5>

                                <div class="cl8 p-b-18">
                                    <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                                        بوسطة محمد أحمد
                                    </a>

                                    <span class="f1-s-3 m-rl-3">
                                        -
                                    </span>

                                    <span class="f1-s-3">
                                        Feb 18
                                    </span>
                                </div>

                                <p class="f1-s-1 cl6 p-b-24">
                                    Duis eu felis id tortor congue consequat. Sed vitae vestibulum enim, et pharetra
                                    magna
                                </p>

                                <a href="blog-detail.html" class="f1-s-1 cl9 hov-cl10 trans-03">
                                    أقرأ المزيد
                                    <i class="m-l-2 fa fa-long-arrow-alt-left"></i>
                                </a>
                            </div>
                        </div>

                        <!-- Item post -->
                        <div class="flex-wr-sb-s p-t-40 p-b-15 how-bor2">
                            <a href="blog-detail.html"
                                class="size-w-8 wrap-pic-w hov1 trans-03 w-full-sr575 m-b-25">
                                <img src="{{asset('site/images/post-45.jpg')}}" alt="IMG">
                            </a>

                            <div class="size-w-9 w-full-sr575 m-b-25">
                                <h5 class="p-b-12">
                                    <a href="blog-detail.html" class="f1-l-1 cl2 hov-cl10 trans-03 respon2">
                                        Success lorem ipsum dolor sit amet consectetur
                                    </a>
                                </h5>

                                <div class="cl8 p-b-18">
                                    <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                                        بوسطة محمد أحمد
                                    </a>

                                    <span class="f1-s-3 m-rl-3">
                                        -
                                    </span>

                                    <span class="f1-s-3">
                                        Feb 18
                                    </span>
                                </div>

                                <p class="f1-s-1 cl6 p-b-24">
                                    Duis eu felis id tortor congue consequat. Sed vitae vestibulum enim, et pharetra
                                    magna
                                </p>

                                <a href="blog-detail.html" class="f1-s-1 cl9 hov-cl10 trans-03">
                                    أقرأ المزيد
                                    <i class="m-l-2 fa fa-long-arrow-alt-left"></i>
                                </a>
                            </div>
                        </div>

                        <!-- Item post -->
                        <div class="flex-wr-sb-s p-t-40 p-b-15 how-bor2">
                            <a href="blog-detail.html"
                                class="size-w-8 wrap-pic-w hov1 trans-03 w-full-sr575 m-b-25">
                                <img src="{{asset('site/images/post-46.jpg')}}" alt="IMG">
                            </a>

                            <div class="size-w-9 w-full-sr575 m-b-25">
                                <h5 class="p-b-12">
                                    <a href="blog-detail.html" class="f1-l-1 cl2 hov-cl10 trans-03 respon2">
                                        Bitcon lorem ipsum dolor sit amet consectetur
                                    </a>
                                </h5>

                                <div class="cl8 p-b-18">
                                    <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                                        بوسطة محمد أحمد
                                    </a>

                                    <span class="f1-s-3 m-rl-3">
                                        -
                                    </span>

                                    <span class="f1-s-3">
                                        Feb 18
                                    </span>
                                </div>

                                <p class="f1-s-1 cl6 p-b-24">
                                    Duis eu felis id tortor congue consequat. Sed vitae vestibulum enim, et pharetra
                                    magna
                                </p>

                                <a href="blog-detail.html" class="f1-s-1 cl9 hov-cl10 trans-03">
                                    أقرأ المزيد
                                    <i class="m-l-2 fa fa-long-arrow-alt-left"></i>
                                </a>
                            </div>
                        </div>

                        <!-- Item post -->
                        <div class="flex-wr-sb-s p-t-40 p-b-15 how-bor2">
                            <a href="blog-detail.html"
                                class="size-w-8 wrap-pic-w hov1 trans-03 w-full-sr575 m-b-25">
                                <img src="{{asset('site/images/post-47.jpg')}}" alt="IMG">
                            </a>

                            <div class="size-w-9 w-full-sr575 m-b-25">
                                <h5 class="p-b-12">
                                    <a href="blog-detail.html" class="f1-l-1 cl2 hov-cl10 trans-03 respon2">
                                        American Bank lorem ipsum dolor sit amet
                                    </a>
                                </h5>

                                <div class="cl8 p-b-18">
                                    <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                                        بوسطة محمد أحمد
                                    </a>

                                    <span class="f1-s-3 m-rl-3">
                                        -
                                    </span>

                                    <span class="f1-s-3">
                                        Feb 18
                                    </span>
                                </div>

                                <p class="f1-s-1 cl6 p-b-24">
                                    Duis eu felis id tortor congue consequat. Sed vitae vestibulum enim, et pharetra
                                    magna
                                </p>

                                <a href="blog-detail.html" class="f1-s-1 cl9 hov-cl10 trans-03">
                                    أقرأ المزيد
                                    <i class="m-l-2 fa fa-long-arrow-alt-left"></i>
                                </a>
                            </div>
                        </div>

                        <!-- Item post -->
                        <div class="flex-wr-sb-s p-t-40 p-b-15 how-bor2">
                            <a href="blog-detail.html"
                                class="size-w-8 wrap-pic-w hov1 trans-03 w-full-sr575 m-b-25">
                                <img src="{{asset('site/images/post-48.jpg')}}" alt="IMG">
                            </a>

                            <div class="size-w-9 w-full-sr575 m-b-25">
                                <h5 class="p-b-12">
                                    <a href="blog-detail.html" class="f1-l-1 cl2 hov-cl10 trans-03 respon2">
                                        Macbook New 12 lorem ipsum dolor sit amet
                                    </a>
                                </h5>

                                <div class="cl8 p-b-18">
                                    <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                                        بوسطة محمد أحمد
                                    </a>

                                    <span class="f1-s-3 m-rl-3">
                                        -
                                    </span>

                                    <span class="f1-s-3">
                                        Feb 18
                                    </span>
                                </div>

                                <p class="f1-s-1 cl6 p-b-24">
                                    Duis eu felis id tortor congue consequat. Sed vitae vestibulum enim, et pharetra
                                    magna
                                </p>

                                <a href="blog-detail.html" class="f1-s-1 cl9 hov-cl10 trans-03">
                                    أقرأ المزيد
                                    <i class="m-l-2 fa fa-long-arrow-alt-left"></i>
                                </a>
                            </div>
                        </div>

                        <!-- Item post -->
                        <div class="flex-wr-sb-s p-t-40 p-b-15 how-bor2">
                            <a href="blog-detail.html"
                                class="size-w-8 wrap-pic-w hov1 trans-03 w-full-sr575 m-b-25">
                                <img src="{{asset('site/images/post-49.jpg')}}" alt="IMG">
                            </a>

                            <div class="size-w-9 w-full-sr575 m-b-25">
                                <h5 class="p-b-12">
                                    <a href="blog-detail.html" class="f1-l-1 cl2 hov-cl10 trans-03 respon2">
                                        هنالك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم
                                    </a>
                                </h5>

                                <div class="cl8 p-b-18">
                                    <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                                        بوسطة محمد أحمد
                                    </a>

                                    <span class="f1-s-3 m-rl-3">
                                        -
                                    </span>

                                    <span class="f1-s-3">
                                        Feb 18
                                    </span>
                                </div>

                                <p class="f1-s-1 cl6 p-b-24">
                                    Duis eu felis id tortor congue consequat. Sed vitae vestibulum enim, et pharetra
                                    magna
                                </p>

                                <a href="blog-detail.html" class="f1-s-1 cl9 hov-cl10 trans-03">
                                    أقرأ المزيد
                                    <i class="m-l-2 fa fa-long-arrow-alt-left"></i>
                                </a>
                            </div>
                        </div>
                    </div>

                    <a href="#" class="flex-c-c size-a-13 bo-all-1 bocl11 f1-m-6 cl6 hov-btn1 trans-03">
                        المزيد
                    </a>
                </div>
            </div>
            @include('website.layouts.leftbar')
        </div>
    </div>
</section>

@endsection
@section('javascript')

@endsection
