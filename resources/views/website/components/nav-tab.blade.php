<div class="tab01 p-b-20">
    <div class="tab01-head how2 how2-cl1 bocl12 flex-s-c m-r-10 m-r-0-sr991">
        <!-- Brand tab -->
        <h3 class="f1-m-2 cl12 tab01-title">
            {{$title}}
        </h3>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#tab{{$index}}-all" role="tab">الكل</a>
            </li>

            @foreach ($categories as $category)
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab{{$index}}-{{$loop->index}}" role="tab">{{$category->name}}</a>
            </li>
            @endforeach

            <li class="nav-item-more dropdown dis-none">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-ellipsis-h"></i>
                </a>

                <ul class="dropdown-menu">

                </ul>
            </li>
        </ul>

        <!--  -->
        <a href="{{$link}}" class="tab01-link f1-s-1 cl9 hov-cl10 trans-03"><!--category-01.html-->
            مشاهدة الكل
            <i class="fs-12 m-l-5 fa fa-caret-left"></i>
        </a>
    </div>


    <!-- Tab panes -->
    <div class="tab-content p-t-35">
        <!-- - -->
        <div class="tab-pane fade show active" id="tab{{$index}}-all" role="tabpanel">
            <div class="row">
                @foreach ($allArticles as $article)

                    @if ($loop->first)
                        <div class="col-sm-6 p-r-25 p-r-15-sr991">
                            @include('website.components.nav-tab-card-large',['article'=>$article])
                        </div>
                    @else
                    <div class="col-sm-6 p-r-25 p-r-15-sr991">
                        @include('website.components.nav-tab-card-small',['article'=>$article])
                    </div>
                    @endif
                @endforeach

            </div>
        </div>

        @foreach ($categories as $category)

        <div class="tab-pane fade" id="tab{{$index}}-{{$loop->index}}" role="tabpanel">
            <div class="row">
                @foreach ($category->getImportantArticles as $article)
                    @if ($loop->first)
                        <div class="col-sm-6 p-r-25 p-r-15-sr991">
                            <!-- Item post -->
                            @include('website.components.nav-tab-card-large',['article'=>$article])

                        </div>
                    @else
                        <div class="col-sm-6 p-r-25 p-r-15-sr991">
                            @include('website.components.nav-tab-card-small',['article'=>$article])
                        </div>
                    @endif
                @endforeach

            </div>
        </div>
        @endforeach

    </div>
</div>
