<!-- Technology -->
<div class="tab01 p-b-20">
    <div class="tab01-head how2 how2-cl3 bocl12 flex-s-c m-r-10 m-r-0-sr991">
        <!-- Brand tab -->
        <h3 class="f1-m-2 cl14 tab01-title">
            تكنولوجى
        </h3>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#tab3-1" role="tab">All</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab3-2" role="tab">Hotels</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab3-3" role="tab">Flight</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab3-4" role="tab">Beachs</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab3-5" role="tab">Culture</a>
            </li>

            <li class="nav-item-more dropdown dis-none">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-ellipsis-h"></i>
                </a>

                <ul class="dropdown-menu">
                    
                </ul>
            </li>
        </ul>

        <!--  -->
        <a href="#" class="tab01-link f1-s-1 cl9 hov-cl10 trans-03"><!--category-01.html-->
            View all
            <i class="fs-12 m-l-5 fa fa-caret-right"></i>
        </a>
    </div>
        

    <!-- Tab panes -->
    <div class="tab-content p-t-35">
        <!-- - -->
        <div class="tab-pane fade show active" id="tab3-1" role="tabpanel">
            <div class="row">
                <div class="col-sm-6 p-r-25 p-r-15-sr991">
                    <!-- Item post -->	
                    <div class="m-b-30">
                        <!-- <a href="#" class="wrap-pic-w hov1 trans-03">
                            <img src="{{asset('site/images/post-14.jpg')}}" alt="IMG">
                        </a> -->
                        <div class="wrap-pic-w pos-relative">
                            <img src="{{asset('site/images/post-14.jpg')}}" alt="IMG">

                            <button class="s-full ab-t-l flex-c-c fs-32 cl0 hov-cl10 trans-03" data-toggle="modal" data-target="#modal-video-01">
                                <span class="fab fa-youtube"></span>
                            </button>
                        </div>

                        <div class="p-t-20">
                            <h5 class="p-b-5">
                                <a href="#" class="f1-m-3 cl2 hov-cl10 trans-03">
                                    You wish lorem ipsum dolor sit amet consectetur 
                                </a>
                            </h5>

                            <span class="cl8">
                                <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                                    Hotels
                                </a>

                                <span class="f1-s-3 m-rl-3">
                                    -
                                </span>

                                <span class="f1-s-3">
                                    Feb 18
                                </span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 p-r-25 p-r-15-sr991">
                    @include('website.components.nav-tab-small')
                </div>
            </div>
        </div>

        <!-- - -->
        <div class="tab-pane fade" id="tab3-2" role="tabpanel">
            <div class="row">
                <div class="col-sm-6 p-r-25 p-r-15-sr991">
                    <!-- Item post -->	
                    <div class="m-b-30">
                        <a href="#" class="wrap-pic-w hov1 trans-03">
                            <img src="{{asset('site/images/post-15.jpg')}}" alt="IMG">
                        </a>

                        <div class="p-t-20">
                            <h5 class="p-b-5">
                                <a href="#" class="f1-m-3 cl2 hov-cl10 trans-03">
                                    You wish lorem ipsum dolor sit amet consectetur 
                                </a>
                            </h5>

                            <span class="cl8">
                                <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                                    Hotels
                                </a>

                                <span class="f1-s-3 m-rl-3">
                                    -
                                </span>

                                <span class="f1-s-3">
                                    Feb 18
                                </span>
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 p-r-25 p-r-15-sr991">
                    @include('website.components.nav-tab-small')
                </div>
            </div>
        </div>
    </div>
</div>