    <!-- Item post -->	
    <div class="m-b-30">
        <a href="{{route('article.single',$article->slug)}}" class="wrap-pic-w hov1 trans-03">
            <img src="{{$article->cover_photo}}" alt="IMG">
        </a>

        <div class="p-t-20">
            <h5 class="p-b-5">
                <a href="{{route('article.single',$article->slug)}}" class="f1-m-3 cl2 hov-cl10 trans-03"><!--blog-detail-01.html-->
                    {{$article->title}}                                         
                </a>
            </h5>

            <span class="cl8">
                <a href="#" class="f1-s-4 cl8 hov-cl10 trans-03">
                    {{$article->category->name}}
                </a>

                <span class="f1-s-3 m-rl-3">
                    -
                </span>

                <span class="f1-s-3">
                    {{$article->created_at->format('M d')}}
                </span>
            </span>
        </div>
    </div>
