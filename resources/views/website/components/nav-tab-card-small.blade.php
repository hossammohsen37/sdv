<div class="flex-wr-sb-s m-b-30">
    <a href="#" class="size-w-1 wrap-pic-w hov1 trans-03"><!--blog-detail-01.html-->
        <img src="{{$article->cover_photo}}" alt="IMG">
    </a>

    <div class="size-w-2">
        <h5 class="p-b-5">
            <a href="{{route('article.single',$article->slug)}}" class="f1-s-5 cl3 hov-cl10 trans-03"><!--blog-detail-01.html-->
                {{$article->title}}
            </a>
        </h5>

        <span class="cl8">
            <a href="{{route('article.single',$article->slug)}}" class="f1-s-6 cl8 hov-cl10 trans-03">
                {{$article->category->name}}
            </a>

            <span class="f1-s-3 m-rl-3">
                -
            </span>

            <span class="f1-s-3">
                {{$article->created_at->format('M d')}}
            </span>
        </span>
    </div>
</div>