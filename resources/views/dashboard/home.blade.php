@extends('dashboard.layouts.main')
@section('content')
    <div class="content sm-gutter">
        <div class="container-fluid padding-25 sm-padding-10">
            <div class="row m-0">
                <!--Visitors Graph-->
                <div class="col-md-6 col-sm-12 col-12">
                    <canvas id="chart"></canvas>
                </div>

                <!--Counter Cards-->
                <div class="col-md-6 col-sm-12 col-12">
                    <div class="row m-0">
                        <div class="col-lg-6 col-12 m-b-10">
                            <!-- START WIDGET -->
                            <div class="widget-8 panel no-border bg-info widget-loader-bar">
                                <div class="panel-heading">
                                    <div class="panel-title hint-text">
                                        <h4 class="text-white" style="line-height: 1;">
                                            عدد المقالات
                                        </h4>
                                    </div>
                                </div>
                                <div class="p-r-20">
                                    <h3 class="p-t-5 text-white">{{$articlesCount}} مقالة</h3>
                                </div>
                            </div>
                            <!-- END WIDGET -->
                        </div>

                        <div class="col-lg-6 col-12 m-b-10">
                            <!-- START WIDGET -->
                            <div class="widget-8 panel no-border bg-success-dark widget-loader-bar">
                                <div class="panel-heading">
                                    <div class="panel-title hint-text">
                                        <h4 class="text-white" style="line-height: 1;">
                                            مقالات تنتظر الموافقة
                                        </h4>
                                    </div>
                                </div>
                                <div class="p-r-20">
                                    <h3 class="p-t-5 text-white">{{$unVerifiedAriclesCount}} مقالة</h3>
                                </div>
                            </div>
                            <!-- END WIDGET -->
                        </div>
                        <div class="col-lg-6 col-12 m-b-10">
                            <!-- START WIDGET -->
                            <div class="widget-8 panel no-border bg-success-dark widget-loader-bar">
                                <div class="panel-heading">
                                    <div class="panel-title hint-text">
                                        <h4 class="text-white" style="line-height: 1;">
                                            عدد زيارات شهر {{date('m/Y',strtotime('-1 months'))}}
                                        </h4>
                                    </div>
                                </div>
                                <div class="p-r-20">
                                    <h3 class="p-t-5 text-white">{{$lastMonthVisits}} زائر</h3>
                                </div>
                            </div>
                            <!-- END WIDGET -->
                        </div>
                        <div class="col-lg-6 col-12 m-b-10">
                            <!-- START WIDGET -->
                            <div class="widget-8 panel no-border bg-success-dark widget-loader-bar">
                                <div class="panel-heading">
                                    <div class="panel-title hint-text">
                                        <h4 class="text-white" style="line-height: 1;">
                                            عدد زيارات هذا الشهر
                                        </h4>
                                    </div>
                                </div>
                                <div class="p-r-20">
                                    <h3 class="p-t-5 text-white">{{$monthVisits}} مشترك</h3>
                                </div>
                            </div>
                            <!-- END WIDGET -->
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid container-fixed-lg bg-white mt-4">
                <!-- START PANEL -->
                <div class="panel panel-transparent">
                    <div class="panel-heading">
                        <div class="panel-title">مقالات اعلى مشاهده</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover demo-table-search" id="tableWithSearch">
                            <thead>
                                <tr>
                                    <th>عنوان المقال </th>
                                    <th>القسم</th>
                                    <th>الكاتب</th>
                                    <th>عدد الزيارات</th>
                                    <th>الفيديو/الصوره</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($topTenArticles as $article)
                                <tr>
                                    <td class="v-align-middle semi-bold">
                                        {{$article->title}}
                                    </td>
                                    <td class="v-align-middle">
                                        {{$article->category->name}}
                                    </td>
                                    <td class="v-align-middle">
                                        <p>{{$article->author_name}}</p>
                                    </td>
                                    <td class="v-align-middle">
                                        <p>
                                           {{$article->visits}}
                                        </p>
                                    </td>
                                    <td class="v-align-middle">
                                        <img src="{{$article->cover_photo}}" style="width: 150px;height: 100px" />
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END PANEL -->
            </div>
        </div>
    </div>
@endsection
@section('javascript')
<script>
    $(document).ready(function(){
        var ctx = document.getElementById('chart').getContext('2d');
        var visitorsChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: {!!$chartDaysVisits['days']!!},
                datasets: [{
                    label: 'رسم تحليلي للزوار الموقع',
                    data: {!!$chartDaysVisits['visits']!!},
                    backgroundColor: [
                        'rgba(255, 99, 132)',
                        'rgba(54, 162, 235)',
                        'rgba(255, 206, 86)',
                        'rgba(75, 192, 192)',
                        'rgba(153, 102, 255)',
                        'rgba(255, 159, 64)',
                        'rgba(75, 192, 192)',
                        'rgba(75, 192, 192)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    });
</script>
@endsection
