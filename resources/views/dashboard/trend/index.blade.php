@extends('dashboard.layouts.main')
@section('content')
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li>
                        <p>التريندات</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->

    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg bg-white">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="panel-title">التريندات</div>
                <div class="export-options-container pull-right"></div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <table class="table table-striped" >
                    <thead>
                        <tr>
                            <th>اسم التريند</th>
                            <th>عدد المقالات</th>
                            <th>الصورة</th>
                            <th>خيارات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($trends as $trend)
                        <tr class="odd gradeX">
                            <td>{{$trend->name}}</td>
                            <td>{{$trend->articles()->count()}}</td>
                            <td>
                                @if ($trend->cover_image_url)
                                    <img src="{{$trend->cover_image_url}}" style="width: 100px;height: 100px">
                                    
                                @endif
                            </td>
                            <td class="ceneter">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                          خيارات <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{route('dashboard.trend.edit',$trend->id)}}">تعديل</a>
                                        </li>
                                        <li>
                                            <a href="{{route('dashboard.trend.show',$trend->id)}}">عرض</a>
                                        </li>
                                        <li>
                                            <form action="{{route('dashboard.trend.destroy',$trend->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-primary">حذف</button>
                                            </form>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </td>
                           
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$trends->links()}}
            </div>
        </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <br><br>

</div>
@endsection