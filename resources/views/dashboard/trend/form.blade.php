@extends('dashboard.layouts.main')
@section('content')
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li>
                            <p>التريندات</p>
                        </li>
                        <li><a href="#" class="active">اضافة تريند</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->

        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading" data-id="achievTab">
                    <div class="panel-title">
                        @if ($trend->id)
                        تعديل تريند
                        @else
                        اضافة تريند
                        @endif
                    </div>
                </div>
                <div class="panel-body" id="achievTab">
                    <!-- <h5>
                        Home Picture
                    </h5> -->
                    @if ($errors->any())
                        <p>Errors</p>
                        @foreach ($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    @endif
                    <form class="form" role="form" action="{{isset($trend->id) ? route('dashboard.trend.update',$trend->id) : route('dashboard.trend.store')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        @isset($trend->id)
                            @method('PUT')
                        @endisset
                        <div class="row">
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>الاسم &nbsp; </label>
                                    <span class="help"></span>
                                    <input type="text"  class="form-control" value="{{$trend->name}}" name="name" required>
                                </div>
                            </div>
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>صورة التريند</label>
                                    <input name="image" class="form-control" id="myId" type="file" multiple />
                                    @if($trend->cover_image_url)
                                    <img src="{{$trend->cover_image_url}}" style="width: 50px; height: 50px; object-fit: contain;" />
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="pull-right">
                            <button class="btn btn-primary btn-cons">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PANEL -->
        </div>

    </div>

@endsection
@section('javascript')
@include('dashboard.layouts.form_scripts')
@include('dashboard.layouts.sweetalert')
@endsection