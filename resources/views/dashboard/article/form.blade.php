@extends('dashboard.layouts.main')
@section('content')
<div class="content">

    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li>
                        <p> المقالات </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->

    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- START PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    اضافه مقال
                </div>
            </div>
            <div class="panel-body">
                <!-- <h5>
                    Home Picture
                </h5> -->
                @if ($errors->any())
                    <p>Errors</p>
                    @foreach ($errors->all() as $error)
                        <p>{{$error}}</p>
                    @endforeach
                @endif
                <form class="form" role="form" action="{{isset($article->id) ? route('dashboard.article.update',$article->id) : route('dashboard.article.store')}}" enctype="multipart/form-data" method="POST">
                    @csrf
                    @isset($article->id)
                        @method('PUT')
                    @endisset
                    <div class="row">
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>اختر القسم</label> <br>
                                <select id="category_id" name="category_id" required class="full-width" data-init-plugin="select2">
                                    @foreach ($categories as $category)
                                        <option value="{{$category->id}}" {{$article->id&&$article->isMyCategory($category->id) ? 'selected' : ''}}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12" id="sub_category_section">
                            <div class="form-group">
                                <label> اختر القسم الفرعى</label> <br>
                                <select id="sub_category" name="sub_category_id" class="full-width" class="form-control">
                                    @if ($article->id)
                                        @if ($article->category->parent->id)
                                            @foreach ($article->category->parent->children as $category)
                                                <option value="{{$category->id}}" {{$category->id == $article->category_id ? 'selected' : ''}}>{{$category->name}}</option>
                                            @endforeach
                                        @endif
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>عنوان المقال</label>
                                <input value="{{$article->title}}" type="text" id="title" class="form-control __analysis" name="title" required placeholder="عنوان المقال">
                            </div>
                        </div>

                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>اسم الكاتب</label>
                                <input value="{{$article->author_name}}" type="text" class="form-control __analysis" name="author_name" required
                                    placeholder="......">
                            </div>
                        </div>

                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>صورة المقال</label>
                                <input name="article_image" class="form-control" id="myId" type="file" multiple />
                                @if($img!=null)
                                <img src="{{$img}}" style="width: 50px; height: 50px; object-fit: contain;" />
                                @endif
                            </div>
                        </div>

                        <div class="col-sm-6 col-12">
                            <div class="form-group form-group-default ">
                                <label>الكلمات الدلالية</label>
                                <input name="tags" class="tagsinput custom-tag-input __analysis" type="text" value="{{$article->tags_string}}"/>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group form-group-default ">
                                <label>كلمة البحث الرئيسية</label>
                                <input name="focus_keyword" id="focus_keyword" class="form-control __analysis" type="text" value="{{$article->focus_keyword}}"/>
                            </div>
                        </div>
                        <div class="col-sm-12 col-12">
                            <div class="form-group">
                                <label> الوصف </label>
                                <textarea name="description" class="form-control __analysis" id="description" rows="3" data-sample-short required>{{$article->description}}</textarea>
                            </div>
                        </div>
                        <div class="col-sm-12 col-12">
                            <div class="form-group">
                                <label>المقال </label>
                                <textarea name="content" class="form-control" id="content" rows="15" data-sample-short required>
                                     {{$article->content}}
                                </textarea>
                            </div>
                        </div>

                        {{-- Analysis --}}
                        <div class="col-12">
                            <div id="accordion" class="row">
                                <div class="card col-12">
                                    <div class="card-header m-0 p-0" id="headingOne">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <h5 class="m-0 p-0 text-left">
                                                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                                        {{-- <span class='align-middle seo-dot green'></span> --}}
                                                        SEO Analysis
                                                    </a>
                                                </h5>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="card-body" style="direction: ltr;text-align: left">
                                        <div class="row">

                                        </div>
                                        <h4>Readabililty <span id="readability-dot" class='align-middle seo-dot grey'></span> </h4>
                                        <ul id="readability-results-list">

                                        </ul>
                                        <h4>SEO <span id="seo-dot" class='align-middle seo-dot grey'></span> </h4>
                                        <ul id="seo-results-list">

                                        </ul>
                                        <div class="pull-right">
                                            <button type="button" id="analyzeButton" class="btn btn-primary btn-cons">Analyze</button>
                                        </div>
                                    </div>
                                </div>
                        </div>

                        <!--SEO-->
                        <div class="col-12">
                            @seoForm($article)
                        </div>
                    </div>
                    {{-- <input hidden="true" type="text" class="form-control" name="verified_by" value="1"> --}}

                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary btn-cons">حفظ</button>
                    </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PANEL -->
    </div>
</div>
@endsection
@section('javascript')
@include('dashboard.layouts.form_scripts')
@include('dashboard.layouts.sweetalert')
<script src="{{asset('js/seo-script.js')}}"></script>
<script>
    $(document).ready(function () {
        $("#category_id").change(function(){
            var category_id = $(this).val();
            $.get('/dashboard/children/'+category_id,function(response){
                var childrenSelect = $('#sub_category');
                if(response != 'false')
                {
                    childrenSelect.html('');
                    childrenSelect.removeProp('disabled');
                    response.forEach(category => {
                        let option = '<option value="'+category.id+'">'+category.name+'</option>';
                        childrenSelect.append(option);
                    });

                }
                else
                {
                    childrenSelect.html('');
                    childrenSelect.prop('disabled',true);
                    console.log(false);
                }
            });
        });
    });
</script>
@endsection
