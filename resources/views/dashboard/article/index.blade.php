@extends('dashboard.layouts.main')
@section('content')
<div class="content">

  <!-- START JUMBOTRON -->
  <div class="jumbotron" data-pages="parallax">
    <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
        <div class="inner">
            <!-- START BREADCRUMB -->
            <ul class="breadcrumb">
                <li>
                    <p>المقالات</p>
                </li>
                {{-- <li><a href="#" class="active">Article View</a></li> --}}
            </ul>
        </div>
    </div>
  </div>
  <!-- END JUMBOTRON -->
   <!-- START CONTAINER FLUID -->
  <div class="container-fluid container-fixed-lg">
    <div class="row">
      <div class="col-md-12">
        <div class="sm-m-l-5 sm-m-r-5">
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                  <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    اضافة الى تريند
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-sm-8">
                      <div class="form-group">
                        <label>اختر التريند</label>
                        <select class="full-width" id="trands" data-init-plugin="select2">
                          @foreach ($trends as $trend)
                            <option value="{{$trend->id}}">{{$trend->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    {{-- <div class="col-4 text-right"> --}}
                      {{-- </div> --}}
                    </div>
                    <button class="btn btn-success btn-cons" id="addToTrendButton">اضافة الى تريند</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END CONTAINER FLUID -->
  <!-- START CONTAINER FLUID -->
  <div class="container-fluid container-fixed-lg bg-white">
    <div class="panel panel-transparent">
      <div class="panel-heading">
        <div class="panel-title">المقالات</div>
        <div class="pull-right">
          <div class="col-xs-12">
            <input type="text" id="search-table" class="form-control pull-right" placeholder="بحث">
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">
        <table class="table table-hover demo-table-search" id="tableWithSearch">
          <thead>
            <tr>
              <th></th>
              <th>عنوان المقال</th>
              <th>القسم</th>
              <th>الكاتب</th>
              <th>تم انشائها بواسطة</th>
              <th>عدد الزيارات</th>
              {{-- <th>المقال</th> --}}
              <th>الفيديو/الصوره</th>
              <th>الموافقة</th>
              <th>خيارات</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($articles as $article)
            <tr>
              <td>
                <div class="checkbox">
                  <input type="checkbox" value="{{$article->id}}" id="checkbox{{$loop->index}}">
                  <label for="checkbox{{$loop->index}}"></label>
                </div>
              </td>
              <td class="v-align-middle"><a href="{{route('article.single',$article->slug)}}" target="_blank">{{$article->title}}</a></td>
              <td class="v-align-middle">{{$article->category->name}}</td>
              <td class="v-align-middle">{{$article->author_name}}</td>
              <td class="v-align-middle">{{$article->createdBy->name}}</td>
              <td class="v-align-middle">{{$article->visits}} </td>
              {{-- <td class="v-align-middle">{!!substr($article->content,0,700)!!}</td> --}}
              <td class="v-align-middle">
                <img src="{{$article->cover_photo}}" style="width: 150px;height: 100px" />
              </td>
              <td>
                @if ($article->verified)
                    <label for="" class="btn btn-success btn-cons">موافق عليها</label>
                @else
                  <label for="" class="btn btn-danger btn-cons"> تنتظر الموافقة </label>
                @endif
              </td>
              <td class="v-align-middle">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                      خيارات <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu" role="menu">
                      <li>
                        <a href="{{route('dashboard.article.show',$article->id)}}">عرض</a>
                      </li>
                      <li>
                          <a href="{{route('dashboard.article.edit',$article->id)}}">تعديل</a>
                      </li>
                      <li>
                          <form action="{{route('dashboard.article.destroy',$article->id)}}" method="post">
                              @csrf
                              @method('DELETE')
                              <button class="btn btn-primary">حذف</button>
                          </form>
                      </li>
                      {{-- @if ($article->trends->count())
                      <li>
                        <form action="{{route('dashboard.trend.removeArticle',$article->id)}}" method="post">
                          @csrf
                          @method('DELETE')
                          <button class="btn btn-primary">حذف من التريند</button>
                        </form>
                      </li>
                      @endif --}}
                  </ul>
                </li>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{$articles->links()}}
      </div>
    </div>
  </div>
  <!-- END CONTAINER FLUID -->
  <br><br>
</div>
@endsection

@section('javascript')
<script>
    var articleCheckboxsValues = [];
    $('#addToTrendButton').on('click', function() {
        var trendId = $('#trands').val();
        // console.log('checkbox values: ', articleCheckboxsValues, 'trand name', trandName);
        if (articleCheckboxsValues.length == 0)
        {
            alert('Must Choose Article From Table');
        }
        else
        {
            $.post('{{route("dashboard.article.addToTrend")}}',{
                _token : '{{csrf_token()}}',
                trend_id : trendId,
                articles : articleCheckboxsValues
            },function(data){
                console.log('done');
                swal({
                  title: 'تم بنجاح',
                  html: data.message,
                  type: 'success'
                })
            });
        }
    });

    $('#tableWithSearch input[type="checkbox"]').on('change', function() {
        if($(this).is(':checked')) {
            articleCheckboxsValues.push($(this).val())
        } else {
            articleCheckboxsValues.splice(articleCheckboxsValues.indexOf($(this).val()), 1);
        }
    });
</script>
@endsection
