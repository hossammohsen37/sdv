@extends('dashboard.layouts.main')
@section('content')
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li>
                        <p>المقال</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->


    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <div class="row">
            <div class="col-md-12">
                <div class="sm-m-l-5 sm-m-r-5">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                        href="#collapseTwo" aria-expanded="false"
                                        aria-controls="collapseTwo">
                                        خيارات المقال
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse in show" role="tabpanel"
                                aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <form class="addActionOnArticle" role="form" action="{{route('dashboard.article.saveActions',$article->id)}}" enctype="multipart/form-data" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <div class="checkbox check-primary">
                                                    <input {{$article->in_slider ? 'checked' : ''}} name="is_slider" type="checkbox" value="1" id="checkbox1">
                                                    <label for="checkbox1">سليدر</label>
                                                </div>
                                                <div class="checkbox check-primary">
                                                    <input {{$article->is_news ? 'checked' : ''}} name="is_news" type="checkbox" value="1" id="checkbo2">
                                                    <label for="checkbo2">الاخبار</label>
                                                </div>
                                                <div class="checkbox check-primary">
                                                    <input {{$article->is_infographic ? 'checked' : ''}} name="is_infographic" type="checkbox" value="1" id="checkbox3">
                                                    <label for="checkbox3">انفوجرافيك</label>
                                                </div>
                                                <div class="checkbox check-primary">
                                                    <input {{$article->is_important ? 'checked' : ''}} name="is_important" type="checkbox" value="1" id="checkbox5">
                                                    <label for="checkbox5">اهم الاحداث</label>
                                                </div>
                                                <div class="checkbox check-primary">
                                                    <input {{$article->is_author ? 'checked' : ''}} name="is_author" type="checkbox" value="1" id="checkbox6">
                                                    <label for="checkbox6">مؤلفون</label>
                                                </div>
                                            </div>
                                            <div class="col-4 text-right">
                                                <button class="btn btn-success btn-cons" type="submit"
                                                    id="addToTrendButton">حفظ</button>
                                            </div>
                                        </div>
                                    </form>
                                    @if(auth()->user()->hasRole(['Super Admin','Moderator'])&&!$article->verified)
                                    <form action="{{route('dashboard.article.verify',$article->id)}}" method="post">
                                        @method('PUT')
                                        @csrf
                                        <div class="col-4 text-right">
                                            <button class="btn btn-complete btn-cons" type="submit"
                                                id="addToTrendButton">موافقة على النشر</button>
                                        </div>
                                    </form>

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTAINER FLUID -->


    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                {{-- <div class="panel-title">Trend Name {if this article belong to TREND}</div> --}}
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="panel panel-default">
                            <div class="panel-heading separator">
                                <div class="panel-title">{{$article->name}}
                                </div>
                            </div>
                            <div class="panel-body">
                                <h3><span class="semi-bold">المقال</span>:</h3>
                                <p>
                                    {!!$article->content!!}
                                </p>
                            </div>
                            <h3><span class="semi-bold">الكاتب</span>: </h3>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <img class="image-responsive-height demo-mw-500" src="{{$article->cover_photo}}"
                            alt="article image">
                        <br><br>
                        <h3><span class="semi-bold">القسم</span>:{{$article->category->name}}</h3>
                        @if($article->is_news == 1)
                        <h3>في :<span class="semi-bold"></span> مقال اخبارى</h3>
                        @endif
                        @if($article->is_important == 1)
                        <h3>في :<span class="semi-bold"></span>  اهم الاحداث</h3>
                        @endif
                        @if($article->in_slider == 1)
                        <h3>في :<span class="semi-bold"></span>  سلايدر</h3>
                        @endif
                        @if($article->is_infographic == 1)
                        <h3>في :<span class="semi-bold"></span>  انفوجرافيك</h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTAINER FLUID -->
</div>
@endsection
