@extends('dashboard.layouts.main')
@section('content')
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li>
                        <p>الاقسام</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->

    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg bg-white">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="panel-title">الاقسام</div>
                <div class="export-options-container pull-right"></div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <table class="table table-striped" >
                    <thead>
                        <tr>
                            <th> اسم القسم</th>
                            <th>القسم الرئيسى</th>
                            <th>عدد المقالات</th>
                            <th>خيارات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                        <tr class="odd gradeX">
                            <td>{{$category->name}}</td>
                            <td>{{$category->parent->name}}</td>
                            <td>{{$category->articles()->count()}}</td>
                            <td class="ceneter">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                          خيارات <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{route('dashboard.categories.edit',$category->id)}}">تعديل</a>
                                        </li>
                                        <li>
                                            <form action="{{route('dashboard.categories.destroy',$category->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-primary">حذف</button>
                                            </form>
                                        </li>
                                        
                                    </ul>
                                </li>
                            </td>
                           
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$categories->links()}}
            </div>
        </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <br><br>

</div>
@endsection