@extends('dashboard.layouts.main')
@section('content')
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li>
                            <p>الاقسام</p>
                        </li>
                        <li><a href="#" class="active">اضافة قسم</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->

        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading" data-id="achievTab">
                    <div class="panel-title">
                        @if ($category->id)
                        تعديل قسم
                        @else
                        اضافة قسم
                        @endif
                    </div>
                </div>
                <div class="panel-body" id="achievTab">
                    <!-- <h5>
                        Home Picture
                    </h5> -->
                    @if ($errors->any())
                        <p>Errors</p>
                        @foreach ($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    @endif
                    <form class="form" role="form" action="{{isset($category->id) ? route('dashboard.categories.update',$category->id) : route('dashboard.categories.store')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        @isset($category->id)
                            @method('PUT')
                        @endisset
                        <div class="row">
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>الاسم &nbsp; </label>
                                    <span class="help"></span>
                                    <input type="text"  class="form-control" value="{{$category->name}}" name="name" required>
                                </div>
                            </div>

                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>القسم الرئيسى للقسم &nbsp; </label>
                                    <select name="parent_id" class="form-control" id="role_select">
                                        <option value="" disabled selected>اختر القسم</option>
                                        @foreach ($parentCategories as $parentCategory)
                                            <option {{$category->parent_id == $parentCategory ? 'selected' : ''}} value="{{$parentCategory->id}}">{{$parentCategory->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{-- <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>وصف المدرب &nbsp; </label>
                                    <span class="help"></span>
                                    <textarea class="form-control"  rows="10" name="description" required></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label> مدرب خاص &nbsp; </label>
                                    <span class="help"></span>
                                    <textarea class="form-control"  rows="10" name="card_text" required></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>الفيديو &nbsp; </label>
                                    <span class="help"></span>
                                    
                                    <input type="file" class="form-control" name="video">
                                </div>
                            </div> --}}
                            {{-- <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label> الصورة الشخصية &nbsp; </label>
                                    <span class="help"></span>
                                    <input type="file" class="form-control" name="image">
                                </div>
                            </div> --}}
                        </div>

                        <div class="pull-right">
                            <button class="btn btn-primary btn-cons">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PANEL -->
        </div>

    </div>

@endsection
@section('javascript')
@include('dashboard.layouts.form_scripts')
@include('dashboard.layouts.sweetalert')
@endsection