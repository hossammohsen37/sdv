@extends('dashboard.layouts.main')
@section('content')
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li>
                        <p>التحكم فى اقسام الصفحة الرئيسية</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->

    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg bg-white">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                {{-- <div class="panel-title">الاقسام</div> --}}
                <div class="export-options-container pull-right"></div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <div class="row">

                
                <div class="panel panel-transparent col-6">
                    <div class="panel-heading">
                        <div class="panel-title">اقسام الصفحة الرئيسية بترتيبها فى الظهور</div>
                        <div class="export-options-container pull-right"></div>
                        <div class="clearfix">قم بسحب الصفوف اعلى و اسفل لتغيير ترتيب الظهور</div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped" >
                            <thead>
                                <tr>
                                    <th> #</th>
                                    <th> اسم القسم</th>
                                    <th>القسم الرئيسى</th>
                                </tr>
                            </thead>
                            <tbody id="sortable">
                                @foreach ($homeCategories as $category)
                                <tr id="{{$category->id}}" class="odd gradeX">
                                    <td> <button class="btn btn-danger hide-btn" id="btn_{{$category->id}}" type="button" > اخفاء </button> </td>
                                    <td>{{$category->name}}</td>
                                    <td>{{$category->parent->name}}</td>
                                   
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <button class="btn btn-submit" id="submit"> حفظ </button>
                        
                    </div>
                </div>
                <div class="panel panel-transparent col-6">
                    <div class="panel-heading">
                        <div class="panel-title"> الاقسام التى ليست فى الصفحة الرئيسية</div>
                        <div class="export-options-container pull-right"></div>
                        <div class="clearfix"> قم بالضغط على اظهار لتجعل القسم يظهر فى الصفحة الرئيسية </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped" >
                            <thead>
                                <tr>
                                    <th> #</th>
                                    <th> اسم القسم</th>
                                    <th>القسم الرئيسى</th>
                                </tr>
                            </thead>
                            <tbody id="hidden">
                                @foreach ($notHomeCategories as $category)
                                <tr id="hidden_{{$category->id}}" class="odd gradeX">
                                    <td> <button type="button" id="btn_{{$category->id}}" class="btn btn-primary choose-btn"> اظهار </button> </td>
                                    <td>{{$category->name}}</td>
                                    <td>{{$category->parent->name}}</td>
                                   
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <br><br>

</div>
@endsection
@section('javascript')
<script>

$( "#sortable" ).sortable();
$( "#sortable" ).disableSelection();

$("#submit").click(function (param) 
{
    var arr = $( "#sortable" ).sortable("toArray");
    console.log(arr);
    
    $.post("{{route('dashboard.categories.order.submit')}}",{
        _token : "{{csrf_token()}}",
        sortingArr : arr 
    },function (data) 
    { 
        if(data=="success")
        {
            swal({
                title: 'تم بنجاح',
                html: 'تم الحفظ بنجاح',
                type: 'success'
            });
        }
    });
    
});

$('.choose-btn').click(function () 
{         
    let id = $(this).attr('id').split('_')[1]; 
    choose(this,id);
});
$('.hide-btn').click(function () 
{ 
    let id = $(this).attr('id').split('_')[1]; 
    hide(this,id);
});

function initButtons() 
{ 
    // $('.choose-btn').off('click');
    // $('.hide-btn').off('click');
    $('.choose-btn').click(function () 
    { 
        let id = $(this).attr('id').split('_')[1]; 
        choose(this,id);
    });
    $('.hide-btn').click(function () 
    { 
        let id = $(this).attr('id').split('_')[1]; 
        hide(this,id);
    });
}

function choose(sender,id) 
{ 
    var row = $("#hidden_"+id);
    row.attr('id',id);
    row.appendTo("#sortable");
    $(sender).html('اخفاء');
    $(sender).attr('class','btn btn-danger hide-btn')
    initButtons();  
}

function hide(sender,id) 
{ 
    console.log(id);
    
    var row = $("#"+id);
    row.attr('id',"hidden_"+id);
    row.appendTo("#hidden");
    $(sender).html('اظهار');
    $(sender).attr('class','btn btn-primary choose-btn');
    initButtons();
}

</script>
@endsection
