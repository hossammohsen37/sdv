<script>
    var values = @json( $values );

    $("input.form-control, textarea.form-control").each(function() {
        $(this).val(values[$(this).attr("name")]);
    });
    $('select.form-control:not([multiple])').each(function() {
        $(this).val(values[$(this).attr("name")]);
    });
    $('select.form-control[multiple]').each(function() {
        var relation  = values[$(this).attr("name").replace('[]','')];
        $(this).val(relation);
    });

    // if (values.lead_requests['id'])
    // {
    //     $("input.form-control.edit-value, textarea.form-control.edit-value").each(function() {
    //         $(this).val(values.lead_requests[$(this).attr("name")]);
    //     });
    //     $('select.form-control.edit-value:not([multiple])').each(function() {
    //         $(this).val(values.lead_requests[$(this).attr("name")]);
    //     });
    //     $('select.form-control.edit-value[multiple]').each(function() {
    //         var relation  = values.lead_requests[$(this).attr("name").replace('[]','')];
    //         $(this).val(relation.map(array => array['id']) );
    //     });
    // }

</script>
