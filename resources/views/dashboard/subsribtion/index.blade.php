@extends('dashboard.layouts.main')
@section('content')
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li>
                        <p>المشتركين</p>
                    </li>
                    {{-- <li><a href="#" class="active">مقالات النظرة الفنية</a></li> --}}
                </ul>
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->

    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg bg-white">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="panel-title">المشتركين</div>
                <div class="export-options-container pull-right"></div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <table class="table table-striped" >
                    <thead>
                        <tr>
                            <th>الايميل</th>
                            <th>تاريخ الاشتراك</th>
                            {{-- <th>خيارات</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($subscribtions as $subscribtion)
                        <tr class="odd gradeX">
                            <td>{{$subscribtion->email}}</td>
                            <td style="direction: ltr;">{{$subscribtion->created_at->format('Y-m-d h:i:s a')}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$subscribtions->links()}}
            </div>
        </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <br><br>

</div>
@endsection