@extends('dashboard.layouts.main')
@section('content')
<div class="content">

    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li>
                        <p> عن الموقع</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->

    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
        <!-- START PANEL -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    عن الموقع 
                </div>
            </div>
            <div class="panel-body">
                <!-- <h5>
                    Home Picture
                </h5> -->
                @if ($errors->any())
                        <p>Errors</p>
                        @foreach ($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    @endif
                    <form class="form" role="form" action="{{route('dashboard.about.store')}}" enctype="multipart/form-data" method="POST">
                      @csrf
                      @isset($article->id)
                            @method('PUT')
                        @endisset
                    <div class="row">
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>  الايميل</label>
                                <input value="{{App\Models\Statics::get('email')}}" type="text" class="form-control" name="email" required placeholder=" الايميل ">
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label> رقم الهاتف</label>
                                <input value="{{App\Models\Statics::get('phone')}}" type="text" class="form-control" name="phone" required placeholder=" رقم الهاتف">
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label> يوتيوب</label>
                                <input value="{{App\Models\Statics::get('youtube')}}" type="text" class="form-control" name="youtube" required placeholder=" يوتيوب">
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label> فيسبوك</label>
                            <input value="{{App\Models\Statics::get('facebook')}}" type="text" class="form-control" name="facebook" required placeholder=" فيسبوك">
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label> تويتر</label>
                            <input value="{{App\Models\Statics::get('twitter')}}" type="text" class="form-control" name="twitter" required placeholder=" تويتر">
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label> سعر الذهب</label>
                                <input value="{{App\Models\Statics::get('gold_price')}}" type="number" class="form-control" name="gold_price" required placeholder=" سعر الذهب">
                            </div>
                        </div>
                        <div class="col-sm-12 col-12">
                            <div class="form-group">
                                <label>عن الموقع </label>
                                     <textarea  name="about" class="form-control" id="" rows="10"
                                        required>
                                        {{App\Models\Statics::get('about')}}
                                    </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary btn-cons">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- END PANEL -->
    </div>
</div>
@endsection
@section('javascript')
@include('dashboard.layouts.form_scripts')
@include('dashboard.layouts.sweetalert')
@endsection
