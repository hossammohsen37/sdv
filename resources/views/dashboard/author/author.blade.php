@extends('dashboard.layouts.main')
@section('content')
<!-- START PAGE CONTENT -->
<div class="content">

    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
      <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
          <div class="inner">
              <!-- START BREADCRUMB -->
              <ul class="breadcrumb">
                  <li>
                      <p>Authors</p>
                  </li>
                  <li><a href="#" class="active">Authors View</a></li>
              </ul>
          </div>
      </div>
    </div>
    <!-- END JUMBOTRON -->

     <!-- START CONTAINER FLUID -->
     <div class="container-fluid container-fixed-lg bg-white">
      <!-- START PANEL -->
      <div class="panel panel-transparent">
        <div class="panel-heading">
          <div class="panel-title">authors Table
          </div>
          <div class="pull-right">
            <div class="col-xs-12">
              <input type="text" id="search-table" class="form-control pull-right" placeholder="Search">
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="panel-body">
          <table class="table table-hover demo-table-search" id="tableWithSearch">
            <thead>
              <tr>
                <th>Name</th>
                <th>E-Mail</th>
                <th>Role</th>
                <th>About</th>
                <th>Articles</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td class="v-align-middle semi-bold">
                  <p>Ahmed Mosaad</p>
                </td>
                <td class="v-align-middle">
                  ahmed@domain.com
                </td>
                <td class="v-align-middle">
                  <p>Admin</p>
                </td>
                <td class="v-align-middle">
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    </p>
                </td>
                <td class="v-align-middle">
                    <a href="article-single.html">View Articles</a>
                </td>
              </tr>
              <tr>
                <td class="v-align-middle semi-bold">
                  <p>Ahmed Mosaad</p>
                </td>
                <td class="v-align-middle">
                  ahmed@domain.com
                </td>
                <td class="v-align-middle">
                  <p>writer</p>
                </td>
                <td class="v-align-middle">
                    <p>
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                        Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    </p>
                </td>
                <td class="v-align-middle">
                    <a href="article-single.html">View Articles</a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <!-- END PANEL -->
    </div>
    <!-- END CONTAINER FLUID -->

  </div>
  <!-- END PAGE CONTENT -->
@endsection
