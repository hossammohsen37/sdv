<!DOCTYPE html>
<html dir="rtl">
    <head>
        @include('dashboard.layouts.head')
    </head>
    <body class="fixed-header dashboard">
        @include('dashboard.layouts.sidebar')
        <!-- START PAGE-CONTAINER -->
        <div class="page-container">
            @include('dashboard.layouts.header')
            <!-- START PAGE CONTENT WRAPPER -->
            <div class="page-content-wrapper">
                @yield('content')
            </div>
            <!-- END PAGE CONTENT WRAPPER -->

        </div>
        <!-- END PAGE CONTAINER -->
        @include('dashboard.layouts.scripts')
        @include('dashboard.layouts.sweetalert')
        @yield('javascript')
    </body>
</html>
