<div class="header">
  <!-- START MOBILE CONTROLS -->
  <!-- LEFT SIDE -->
  <div class="pull-left full-height visible-sm visible-xs">
      <!-- START ACTION BAR -->
      <div class="sm-action-bar">
          <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
              <span class="icon-set menu-hambuger"></span>
          </a>
      </div>
      <!-- END ACTION BAR -->
  </div>

  <div class=" pull-left sm-table">
      <div class="header-inner">
          <a href="{{route('home')}}" target="_blank" class="brand inline">
              <img src="{{asset('site/images/icons/logo-01.png')}}" alt="logo"
                data-src="{{asset('site/images/icons/logo-01.png')}}" 
                data-src-retina="{{asset('site/images/icons/logo-01.png')}}"
                style="width: 80px; height: 40px; object-fit: cover;"
                >
          </a>
      </div>
  </div>

  <div class=" pull-right">
      <!-- START User Info-->
      <div class="visible-lg visible-md m-t-10">
          <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
              <span class="semi-bold">{{auth()->user()->name}}</span>{{-- <span class="text-master">Nest</span> --}}
          </div>
          <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if (Auth::user()->profile_pic)
                    <span class="thumbnail-wrapper d32 circular inline m-t-5">
                        <img src="{{Auth::user()->profile_pic}}" alt=""
                            data-src="{{Auth::user()->profile_pic}}"
                            data-src-retina="{{Auth::user()->profile_pic}}" width="32" height="32">
                    </span>
                    @else
                    <span class="thumbnail-wrapper d32 circular inline m-t-5">
                        <img src="{{asset('dashboard-assets/img/profiles/avatar.jpg')}}" alt=""
                            data-src="{{asset('dashboard-assets/img/profiles/avatar.jpg')}}"
                            data-src-retina="{{asset('dashboard-assets/img/profiles/avatar.jpg')}}" width="32" height="32">
                    </span>
                    @endif

              </button>
              <ul class="dropdown-menu profile-dropdown" role="menu">
                    <li><a href="#"><i class="pg-settings_small"></i> Settings</a>
                    </li>
                    <li><a href="#"><i class="pg-outdent"></i> Feedback</a>
                    </li>
                    <li><a href="#"><i class="pg-signals"></i> Help</a>
                    </li>
                    <li class="bg-master-lighter">
                        <form action="{{route('dashboard.logout')}}" method="POST">
                            @csrf
                            <button class="btn">
                                <i class="pg-power"></i>
                                تسجيل الخروج
                            </button>
                        </form>
                        {{-- <form action="{{route('logout')}}" method="POST">
                            @csrf
                            <button type="submit">
                                <span class="pull-right">
                                </span>
                                <span class="pull-left"></span>
                            </button>
                        </form> --}}
                    </li>
              </ul>
          </div>
      </div>
      <!-- END User Info-->
  </div>
</div>
