<!-- BEGIN SIDEBPANEL-->
<nav class="page-sidebar" data-pages="sidebar">

    <!-- BEGIN SIDEBAR MENU HEADER-->
    <div class="sidebar-header">
        <a">
            <img src="{{asset('site/images/icons/logo-02.png')}}" alt="logo" class="brand"
            data-src="{{asset('site/images/icons/logo-02.png')}}" data-src-retina="{{asset('site/images/icons/logo-02.png')}}"
            style="height: 75%; width: 80px; object-fit: contain;">
        </a>
        <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar">
            <i class="fa fs-12"></i>
        </button>
    </div>
    <!-- END SIDEBAR MENU HEADER-->

    <!-- START SIDEBAR MENU -->
    <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
            <li class="m-t-30 open">
                <a href="{{route('dashboard.home')}}" class="detailed">
                    <span class="title">الصفحة الرئيسية</span>
                </a>
                <span class="icon-thumbnail bg-success"><i class="pg-home"></i></span>
            </li>

            {{-- <li class="">
                <a href="javascript:;"><span class="title"> بيانات الصفحة الرئيسية </span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="pg-layouts"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">بيانات الصفحة الرئيسية </a>
                        <span class="icon-thumbnail">H</span>
                    </li>
                </ul>
            </li> --}}

            {{-- <li class="">
                <a href="javascript:;"><span class="title">النظرة الفنية</span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="pg-layouts"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="#">مقالات النظرة الفنية</a>
                        <span class="icon-thumbnail">P</span>
                    </li>
                    <li class="">
                        <a href="#">اضافة مقال</a>
                        <span class="icon-thumbnail">+</span>
                    </li>
                </ul>
            </li> --}}
            @if (auth()->user()->hasRole('Super Admin'))
            <li class="">
                <a href="javascript:;"><span class="title"> المستخدمين </span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="pg-layouts"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="{{route('dashboard.users.index')}}">المستخدمين</a>
                        {{-- <span class="icon-thumbnail">P</span> --}}
                    </li>
                    <li class="">
                        <a href="{{route('dashboard.users.create')}}">اضافة مستخدم</a>
                        {{-- <span class="icon-thumbnail">P</span> --}}
                    </li>

                </ul>
            </li>
            @endif
            <li class="">
                <a href="{{route('dashboard.users.edit',auth()->user()->id)}}" class="detailed">
                    <span class="title">
                        حسابى
                    </span>
                </a>
                <span class="icon-thumbnail"><i class="pg-layouts"></i></span>
            </li>
            @if (auth()->user()->hasRole('Super Admin'))
            <li class="">
                <a href="javascript:;"><span class="title"> الاقسام </span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="pg-layouts"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="{{route('dashboard.categories.index')}}">الاقسام</a>
                        {{-- <span class="icon-thumbnail">P</span> --}}
                    </li>
                    <li class="">
                        <a href="{{route('dashboard.categories.order')}}">التحكم فى اقسام الصفحة الرئيسية</a>
                        {{-- <span class="icon-thumbnail">P</span> --}}
                    </li>
                    <li class="">
                        <a href="{{route('dashboard.categories.create')}}">اضافة قسم</a>
                        {{-- <span class="icon-thumbnail">P</span> --}}
                    </li>

                </ul>
            </li>
            @endif
            <li class="">
                <a href="javascript:;"><span class="title"> المقالات </span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="pg-layouts"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="{{route('dashboard.article.index')}}">المقالات</a>
                        <span class="icon-thumbnail">A</span>
                    </li>
                    <li class="">
                        <a href="{{route('dashboard.article.create')}}">اضافه مقال</a>
                        <span class="icon-thumbnail">+</span>
                    </li>

                </ul>
            </li>
            @if (auth()->user()->hasRole('Super Admin'))
            <li class="">
                <a href="javascript:;"><span class="title"> الموقع </span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="pg-layouts"></i></span>
                <ul class="sub-menu">
                    <li>
                        <a href="{{route('dashboard.about.create')}}">عن الموقع</a>
                        <span class="icon-thumbnail">A</span>
                    </li>
                    <li>
                        <a href="{{route('__visitlog__')}}">زيارات الموقع</a>
                        <span class="icon-thumbnail">A</span>
                    </li>
                </ul>
            </li>
            @endif
            <li class="">
                <a href="{{route('dashboard.subscribtion.index')}}" class="detailed">
                    <span class="title"> المشتركين </span></a>
                <span class="icon-thumbnail"><i class="pg-layouts"></i></span>

            </li>
            @if (auth()->user()->hasRole(['Super Admin','Moderator']))

            <li class="">
                <a href="javascript:;"><span class="title"> التريندات </span>
                <span class=" arrow"></span></a>
                <span class="icon-thumbnail"><i class="pg-layouts"></i></span>
                <ul class="sub-menu">
                    <li class="">
                        <a href="{{route('dashboard.trend.index')}}">التريندات</a>
                        <span class="icon-thumbnail">A</span>
                    </li>
                    <li class="">
                        <a href="{{route('dashboard.trend.create')}}">اضافه تريند</a>
                        <span class="icon-thumbnail">+</span>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
        <div class="clearfix"></div>
    </div>
    <!-- END SIDEBAR MENU -->

</nav>
<!-- END SIDEBAR -->
<!-- END SIDEBPANEL-->
