@if($errors->any())
<script>
	swal({
		title: 'بالرجاء ادخال البيانات صحيحة',
		html: '{!! implode('<br>',$errors->all()) !!}',
		type: 'error'
	})
</script> 
@endif

@if (session('success'))
<script>
	swal({
		title: 'تم بنجاح',
		html: '{{ session('success') }}',
		type: 'success'
	})
</script>
@elseif (session('error'))
	<script>
        swal({
            title: 'خطأ',
            html: '{{ session('error') }}',
            type: 'error'
        })
	</script>
@endif
@if(auth()->user()->delete_requested)
	<script>
		swal({
			title: "تم طلب حذف حسابك",
			text: "تم طلب حذف حسابك من سوبر ادمن هل انت موافق؟",
			type: "warning",
			showCancelButton: true,
			cancelButtonText: "لا",
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "نعم, اريد الحذف"
		}).then(result => {
			$.post('{{route('dashboard.users.confirmDelete')}}',
			{
				_token : '{{csrf_token()}}',
				value : result.value
			},function(response){
				// console.log(response);
				
				if (response == 'true') 
				{
					// console.log('true');
					
					location.reload();
				}
				else
				{
					
				}
			});
		});
	</script>
@endif
<script>
	$('button').parent('form:has(input[value="DELETE"]):not(".no-confirm")').click(function( e ) {
		e.preventDefault();
		var form = $(this).parents('form');
		swal({
			title: "هل تريد الحذف؟",
			text: "لا يمكن الرجوع فى الحذف",
			type: "warning",
			showCancelButton: true,
			cancelButtonText: "لا",
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "نعم, اريد الحذف"
		}).then(result => {
			if (result.value) $(this).submit();
		});
	});
</script>