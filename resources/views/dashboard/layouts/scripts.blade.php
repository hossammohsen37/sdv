<script src="{{asset('dashboard-assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/modernizr.custom.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/boostrapv3/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/jquery/jquery-easy.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/jquery-bez/jquery.bez.min.js')}}"></script>
<script src="{{asset('dashboard-assets/plugins/jquery-ios-list/jquery.ioslist.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/jquery-actual/jquery.actual.min.js')}}"></script>
<script src="{{asset('dashboard-assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard-assets/plugins/bootstrap-select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard-assets/plugins/classie/classie.js')}}"></script>
<script src="{{asset('dashboard-assets/plugins/switchery/js/switchery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/nvd3/lib/d3.v3.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/nvd3/nv.d3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/nvd3/src/utils.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/nvd3/src/tooltip.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/nvd3/src/interactiveLayer.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/nvd3/src/models/axis.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/nvd3/src/models/line.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/nvd3/src/models/lineWithFocusChart.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/mapplic/js/hammer.js')}}"></script>
<script src="{{asset('dashboard-assets/plugins/mapplic/js/jquery.mousewheel.js')}}"></script>
<script src="{{asset('dashboard-assets/plugins/mapplic/js/mapplic.js')}}"></script>
<script src="{{asset('dashboard-assets/plugins/summernote/js/summernote.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/rickshaw/rickshaw.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard-assets/plugins/dropzone/dropzone.min.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script src="{{asset('dashboard-assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{asset('dashboard-assets/plugins/jquery-metrojs/MetroJs.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/js/sweetalert2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard-assets/plugins/dropzone/dropzone.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dashboard-assets/plugins/jquery-inputmask/jquery.inputmask.min.js')}}"></script>

<!--will remove this-->
<script src="{{asset('dashboard-assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<script src="{{asset('dashboard-assets/plugins/skycons/skycons.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('dashboard-assets/plugins/jquery-autonumeric/autoNumeric.js')}}"></script>


<!---->


<!--CK Editor-->
<script src="https://cdn.ckeditor.com/4.14.0/full/ckeditor.js"></script>


<script src="{{asset('dashboard-assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/plugins/datatables-responsive/js/datatables.responsive.js')}}" type="text/javascript" ></script>
<script src="{{asset('dashboard-assets/plugins/datatables-responsive/js/lodash.min.js')}}" type="text/javascript" ></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" crossorigin="anonymous"></script>

<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{asset('dashboard-assets/js/form_elements.js')}}"></script>
<script src="{{asset('dashboard-assets/js/datatables.js')}}" type="text/javascript"></script>
<script src="{{asset('dashboard-assets/pages/js/pages.js')}}"></script>
<script src="{{asset('dashboard-assets/js/scripts.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
{{-- <script src="{{mix('js/worker.js')}}"></script>
<script type="module" src="{{mix('js/main.js')}}"></script>
<script src="{{mix('js/test.js')}}"></script> --}}
<!-- END CORE TEMPLATE JS -->

