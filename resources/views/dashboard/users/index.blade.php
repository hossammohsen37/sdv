@extends('dashboard.layouts.main')
@section('content')
<div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
        <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <div class="inner">
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li>
                        <p>المستخدمين</p>
                    </li>
                    {{-- <li><a href="#" class="active">مقالات النظرة الفنية</a></li> --}}
                </ul>
            </div>
        </div>
    </div>
    <!-- END JUMBOTRON -->

    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg bg-white">
        <div class="panel panel-transparent">
            <div class="panel-heading">
                <div class="panel-title">المستخدمين</div>
                <div class="export-options-container pull-right"></div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                <table class="table table-striped" >
                    <thead>
                        <tr>
                            <th>اسم المستخدم</th>
                            <th>الايميل</th>
                            <th>النوع</th>
                            <th>خيارات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr class="odd gradeX">
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role_name}}</td>
                            <td class="ceneter">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                          خيارات <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{route('dashboard.users.edit',$user->id)}}">تعديل</a>
                                        </li>
                                        <li>
                                            @if (auth()->user()->hasRole('Super Admin'))
                                            <form action="{{route('dashboard.users.destroy',$user->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-primary">حذف</button>
                                            </form>
                                            @endif
                                        </li>
                                        <li>
                                        </li>
                                    </ul>
                                </li>
                            </td>
                           
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$users->links()}}
            </div>
        </div>
    </div>
    <!-- END CONTAINER FLUID -->
    <br><br>

</div>
@endsection