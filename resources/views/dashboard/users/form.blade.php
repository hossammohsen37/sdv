@extends('dashboard.layouts.main')
@section('content')
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li>
                            <p>المستخدمين</p>
                        </li>
                        <li><a href="#" class="active">اضافة مستخدم</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->

        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg">
            <!-- START PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading" data-id="achievTab">
                    <div class="panel-title">
                        اضافة مستخدم
                    </div>
                </div>
                <div class="panel-body" id="achievTab">
                    <!-- <h5>
                        Home Picture
                    </h5> -->
                    @if ($errors->any())
                        <p>Errors</p>
                        @foreach ($errors->all() as $error)
                            <p>{{$error}}</p>
                        @endforeach
                    @endif
                    <form class="form" role="form" action="{{isset($user->id) ? route('dashboard.users.update',$user->id) : route('dashboard.users.store')}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        @isset($user->id)
                            @method('PUT')
                        @endisset
                        <div class="row">

                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>الاسم &nbsp; </label>
                                    <span class="help"></span>
                                    <input type="text"  class="form-control" value="{{$user->name}}" name="name" required>
                                </div>
                            </div>
                            
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>البريد الالكترونى &nbsp; </label>
                                    <span class="help"></span>
                                    <input type="email" class="form-control"  name="email"  required >
                                </div>
                            </div>
                            
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>كلمة المرور &nbsp; </label>
                                    <span class="help"></span>
                                    <input type="password" class="form-control"  name="password" @if(!$user->id)required @endif>
                                </div>
                            </div>
                            
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>تأكيد كلمة المرور &nbsp; </label>
                                    <span class="help"></span>
                                    <input type="password" class="form-control"  name="password_confirmation"  @if(!$user->id)required @endif>
                                </div>
                            </div>
                            @if(auth()->user()->hasRole(['Super Admin'])&& $user->id != auth()->user()->id)
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>نوع المستخدم &nbsp; </label>
                                    <select name="role_id" class="form-control" id="role_select" required>
                                        <option value="">اختر المستخدم</option>
                                        @foreach ($roles as $role)
                                            <option {{$user->hasRole($role->id) ? 'selected' : ''}} value="{{$role->id}}">{{$role->name_ar}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif

                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>عن المستخدم &nbsp; </label>
                                    <span class="help"></span>
                                    <textarea class="form-control"  rows="10" name="about"></textarea>
                                </div>
                            </div>
                            {{-- <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>وصف المدرب &nbsp; </label>
                                    <span class="help"></span>
                                    <textarea class="form-control"  rows="10" name="description" required></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label> مدرب خاص &nbsp; </label>
                                    <span class="help"></span>
                                    <textarea class="form-control"  rows="10" name="card_text" required></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label>الفيديو &nbsp; </label>
                                    <span class="help"></span>
                                    
                                    <input type="file" class="form-control" name="video">
                                </div>
                            </div> --}}
                            <div class="col-sm-12 col-12">
                                <div class="form-group">
                                    <label> الصورة الشخصية &nbsp; </label>
                                    <span class="help"></span>
                                    <input type="file" class="form-control" name="profile_pic">
                                </div>
                            </div>
                        </div>

                        <div class="pull-right">
                            <button class="btn btn-primary btn-cons">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END PANEL -->
        </div>

    </div>

@endsection
@section('javascript')
@include('dashboard.layouts.form_scripts')
@include('dashboard.layouts.sweetalert')
@include('dashboard.js.values',['values'=>$user])
@endsection
