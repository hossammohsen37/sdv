<?php

use Illuminate\Http\Request;

Route::name('dashboard.')->prefix('dashboard')->group(function () {
    Auth::routes();
    Route::get('/test',function(){
        $tags = 'hello,test,shampo';
        $tagsArr = explode(',',$tags);
        foreach($tagsArr as $tag)
        {
            dd($tag);
        }
        dd($tagsArr);
    });
    Route::post('/uploadImage', 'ImageController@editorUpload');
    Route::group(['middleware'=>'auth'], function () 
    {
        Route::get('/','HomeController@home')->name('home');
        Route::get('children/{category}','CategoriesController@getChildren')->name('categories.getChildren');
        Route::get('categories/order','CategoriesController@orderIndex')->name('categories.order');
        Route::post('categories/order','CategoriesController@orderSubmit')->name('categories.order.submit');
        Route::resource('categories', 'CategoriesController');
        Route::post('users/confirmDelete','UserController@confirmDelete' )->name('users.confirmDelete');    
        Route::resource('users', 'UserController');
        Route::post('saveAction/{article}','ArticleController@saveArticle')->name('article.saveActions');
        Route::post('articles/addToTrend','ArticleController@addToTrend')->name('article.addToTrend');
        Route::put('articles/verifyArticle/{article}','ArticleController@verify')->name('article.verify');
        Route::resource('article', 'ArticleController');
        Route::delete('trend/{trend}/removeArticle/{article}','TrendController@removeArticle')->name('trend.removeArticle');
        Route::post('trend/removeArticles','TrendController@removeArticles')->name('trend.removeArticles');
        Route::resource('trend', 'TrendController');
        Route::resource('subscribtion', 'SubscribtionsController');        

        Route::get('single', function () {
            return view('dashboard.article.single');
        });
        Route::resource('about', 'AboutController');
    });
});
