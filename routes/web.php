<?php

Route::get('/','HomeController@index')->name("home");
Route::get('/search', 'ArticleController@search')->name('search');
Route::get('/articles/', 'ArticleController@search')->name('article.tag');
Route::get('section/{categorySlug}','ArticleController@index')->name('section.single');
Route::get('article/view/{articleSlug}','ArticleController@single')->name('article.single');
Route::get('articles/top-important/{categorySlug?}','ArticleController@mostImportant')->name('article.mostImportant');
Route::get('profile/{id}','HomeController@profile')->name('author.profile');
Route::get('trending/{trendSlug}','ArticleController@trendIndex')->name('trend.index');
Route::post('/subscribe', 'HomeController@subscribe')->name('subscribe');