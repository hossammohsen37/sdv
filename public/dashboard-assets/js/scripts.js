(function($) {

    'use strict';

    $(document).ready(function() {
        // Initializes search overlay plugin.
        // Replace onSearchSubmit() and onKeyEnter() with
        // your logic to perform a search and display results
        $(".list-view-wrapper").scrollbar();

        $('[data-pages="search"]').search({
            searchField: '#overlay-search',
            closeButton: '.overlay-close',
            suggestions: '#overlay-suggestions',
            brand: '.brand',
            onSearchSubmit: function(searchString) {
                console.log("Search for: " + searchString);
            },
            onKeyEnter: function(searchString) {
                console.log("Live search for: " + searchString);
                var searchField = $('#overlay-search');
                var searchResults = $('.search-results');

                clearTimeout($.data(this, 'timer'));
                searchResults.fadeOut("fast");
                var wait = setTimeout(function() {

                    searchResults.find('.result-name').each(function() {
                        if (searchField.val().length != 0) {
                            $(this).html(searchField.val());
                            searchResults.fadeIn("fast");
                        }
                    });
                }, 500);
                $(this).data('timer', wait);

            }
        });


        $("#unitType").change(function() {
            var unitOption;
            $(this).find("option:selected").each(function() {
                unitOption = 'type_'+$(this).attr("value");
                if (unitOption) {
                    $("." + unitOption).show();
                }
            });


            var unitsValue = $(this).val();
            var removeUnit = $('.select2-search-choice-close');
            removeUnit.on('click', function(e) {
                var unitName = this.parentElement.innerText;

                if(unitName === '    Villa    ') {
                    $(".type_1").hide();
                }
                if(unitName === '    Apartments    ') {
                    return $(".type_3").hide();
                }
                if(unitName === '    Town House    ') {
                    return $(".type_2").hide();
                }
            });

        }).change();


        $("#homeCover").change(function() {
            $(this).find("option:selected").each(function() {
                var unitOption = $(this).attr("value");
                if(unitOption) {
                    $(".choosenCover").not("." + unitOption).hide();
                    $("." + unitOption).slideDown();
                } else {
                    $('.choosenCover').slideUp();
                }
            });
        }).change();

        $("#projectCover").on('change', function() {
            $('div.choosenCover').fadeOut('fast');
            $('div.choosenCover.title').fadeIn('fast');
            var unitValue = $(this).val();
            if(unitValue == 'img') {
                $('div.img').slideDown('slow');
            }
            if(unitValue == 'img3D') {
                $('div.img3D').slideDown('fast');
            }
            if(unitValue == 'slider') {
                $('div.slider').slideDown('fast');
            }
            if(unitValue == 'video') {
                $('div.video').slideDown('fast');
            }
        });



        // SEO Navs
        $('a[data-toggle="tab"]').on('click', function (e) {
            var tab = $('a[data-toggle="tab"]');
            var tabContent = $('.tab-pane');
            var target = $(this)[0].attributes.href.value;

            tab.removeClass('active');
            tabContent.removeClass('show active');

            $(this).addClass('active');
            $(target).removeClass('hide');
            $(target).addClass('show active');
        });
    });


    $('.panel-collapse label').on('click', function(e){
        e.stopPropagation();
    });






})(window.jQuery);


// Add Articles To Trend { Articles Page }
// document.querySelector('#')
