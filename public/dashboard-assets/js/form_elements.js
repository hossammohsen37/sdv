/* ============================================================
 * Form Elements
 * This file applies various jQuery plugins to form elements
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */
(function ($) {
    "use strict";

    var getBaseURL = function () {
        var url = document.URL;
        return url.substr(0, url.lastIndexOf("/"));
    };

    $(document).ready(function () {
        //Multiselect - Select2 plug-in
        $(".multi").val(["Jim", "Lucy"]).select2();

        //Date Pickers
        $("#datepicker-range, #datepicker-component, #datepicker-component2").datepicker();

        $("#datepicker-embeded").datepicker({
            daysOfWeekDisabled: "0,1",
        });

        // $('#daterangepicker').daterangepicker({
        //     timePicker: false,
        //     format: 'MM/DD/YYYY h:mm A'
        // }, function(start, end, label) {
        //     console.log(start.toISOString(), end.toISOString(), label);
        // });

        /* Time picker
         * https://github.com/m3wolf/bootstrap3-timepicker
         */
        // $('#timepicker').timepicker().on('show.timepicker', function(e) {
        //     var widget = $('.bootstrap-timepicker-widget');
        //     widget.find('.glyphicon-chevron-up').removeClass().addClass('pg-arrow_maximize');
        //     widget.find('.glyphicon-chevron-down').removeClass().addClass('pg-arrow_minimize');
        // });

        //Autonumeric plug-in - automatic addition of dollar signs,etc controlled by tag attributes
        // $('.autonumeric').autoNumeric('init');

        //Drag n Drop up-loader
        $("div.myId").dropzone({
            url: "/file/post",
        });
        //Single instance of tag inputs - can be initiated with simply using data-role="tagsinput" attribute in any input field
        $(".custom-tag-input").tagsinput();

        // $('#summernote').summernote({
        //     height: 200,
        //     onfocus: function(e) {
        //         $('body').addClass('overlay-disabled');
        //     },
        //     onblur: function(e) {
        //         $('body').removeClass('overlay-disabled');
        //     }
        // });


        CKEDITOR.replace("content", {
            filebrowserBrowseUrl: '/browser/browse.php',
            filebrowserUploadUrl: '/dashboard/uploadImage',
            filebrowserWindowWidth: '640',
            filebrowserWindowHeight: '480',
            language: 'ar',
            toolbar: [
                { name: 'clipboard', items: ['PasteFromWord', '-', 'Undo', 'Redo']},
                { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'RemoveFormat', 'Subscript', 'Superscript']},
                { name: 'links', items: ['Link', 'Unlink']},
                { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
                { name: 'insert', items: ['Image', 'Table']},
                { name: 'editing', items: ['Scayt']},
                '/',
                { name: 'styles', items: ['Format', 'Font', 'FontSize']},
                { name: 'colors', items: ['TextColor', 'BGColor', 'CopyFormatting']},
                { name: 'align', items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
                { name: 'document', items: ['Print', 'Source']}
            ],

            // Enabling extra plugins, available in the full-all preset: https://ckeditor.com/cke4/presets
            extraPlugins: 'font,justify,print,pastefromword,liststyle',
        });
    });
})(window.jQuery);
