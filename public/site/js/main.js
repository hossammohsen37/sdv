
(function ($) {
    "use strict";

    /*==================================================================
    [ Load page ]*/
    try {
        $(".animsition").animsition({
            inClass: 'fade-in',
            outClass: 'fade-out',
            inDuration: 1500,
            outDuration: 800,
            linkElement: '.animsition-link',
            loading: true,
            loadingParentElement: 'html',
            loadingClass: 'animsition-loading-1',
            loadingInner: '<div class="loader05"></div>',
            timeout: false,
            timeoutCountdown: 5000,
            onLoadEvent: true,
            browser: [ 'animation-duration', '-webkit-animation-duration'],
            overlay : false,
            overlayClass : 'animsition-overlay-slide',
            overlayParentElement : 'html',
            transition: function(url){ window.location.href = url; }
        });
    } catch(er) {console.log(er);}


    /*==================================================================
    [ Back to top ]*/
    try {
        var windowH = $(window).height()/2;

        $(window).on('scroll',function(){
            if ($(this).scrollTop() > windowH) {
                $("#myBtn").addClass('show-btn-back-to-top');
            } else {
                $("#myBtn").removeClass('show-btn-back-to-top');
            }
        });

        $('#myBtn').on("click", function(){
            $('html, body').animate({scrollTop: 0}, 300);
        });
    } catch(er) {console.log(er);}


    /*==================================================================
    [ Fixed menu ]*/
    try {
        var posNav = $('.wrap-main-nav').offset().top;
        var menuDesktop = $('.container-menu-desktop');
        var mainNav = $('.main-nav');
        var lastScrollTop = 0;
        var st = 0;

        $(window).on('scroll',function(){
            fixedHeader();
        });

        $(window).on('resize',function(){
            fixedHeader();
        });

        $(window).on('load',function(){
            fixedHeader();
        });

        var fixedHeader = function() {
            st = $(window).scrollTop();

            if(st > posNav + mainNav.outerHeight()) {
                $(menuDesktop).addClass('fix-menu-desktop');
                $(mainNav).addClass('show-main-nav');
            }
            else if(st <= posNav) {
                $(menuDesktop).removeClass('fix-menu-desktop');
                $(mainNav).addClass('show-main-nav');
            }

            lastScrollTop = st;
        };

    } catch(er) {console.log(er);}

    /*==================================================================
    [ Menu mobile ]*/
    try {
        $('.btn-show-menu-mobile').on('click', function(){
            $(this).toggleClass('is-active');
            $('.menu-mobile').slideToggle();
        });

        var arrowMainMenu = $('.arrow-main-menu-m');

        for(var i=0; i<arrowMainMenu.length; i++){
            $(arrowMainMenu[i]).on('click', function(){
                $(this).parent().find('.sub-menu-m').slideToggle();
                $(this).toggleClass('turn-arrow-main-menu-m');
            })
        }

        $(window).on('resize',function(){
            if($(window).width() >= 992){
                if($('.menu-mobile').css('display') === 'block') {
                    $('.menu-mobile').css('display','none');
                    $('.btn-show-menu-mobile').toggleClass('is-active');
                }

                $('.sub-menu-m').each(function(){
                    if($(this).css('display') === 'block') {
                        $(this).css('display','none');
                        $(arrowMainMenu).removeClass('turn-arrow-main-menu-m');
                    }
                });

            }
        });
    } catch(er) {console.log(er);}


    /*==================================================================
    [ Respon tab01 ]*/
    try {
        $('.tab01').each(function(){
            var tab01 = $(this);
            var navTabs = $(this).find('.nav-tabs');
            var dropdownMenu = $(tab01).find('.nav-tabs>.nav-item-more .dropdown-menu');
            var navItem = $(tab01).find('.nav-tabs>.nav-item');

            var navItemSize = [];
            var size = 0;
            var wNavItemMore = 0;

            $(window).on('load', function(){
                navItem.each(function(){
                    size += $(this).width();
                    navItemSize.push(size);
                });

                responTab01();
            });

            $(window).on('resize', function(){
                responTab01();
            })

            var responTab01 = function() {
                if(navTabs.width() <= navItemSize[navItemSize.length - 1] + 1) {
                    $(tab01).find('.nav-tabs>.nav-item-more').removeClass('dis-none');
                }
                else {
                    $(tab01).find('.nav-tabs>.nav-item-more').addClass('dis-none');
                }

                wNavItemMore = $(tab01).find('.nav-tabs>.nav-item-more').hasClass('dis-none')? 0 : $(tab01).find('.nav-tabs>.nav-item-more').width();

                for(var i=0 ; i<navItemSize.length ; i++) {

                    if(navTabs.width() - wNavItemMore <= navItemSize[i] + 1) {
                        $(tab01).find('.nav-tabs .nav-item').remove();

                        for(var j=i-1 ; j >= 0 ; j--) {
                            $(navTabs).prepend($(navItem[j]).clone());
                        }

                        for(var j=i ; j < navItemSize.length ; j++) {
                            $(dropdownMenu).append($(navItem[j]).clone());
                        }

                        break;
                    }
                    else {
                        $(tab01).find('.nav-tabs .nav-item').remove();

                        for(var j=i ; j >= 0 ; j--) {
                            $(navTabs).prepend($(navItem[j]).clone());
                        }
                    }
                }
            };
        });
    } catch(er) {console.log(er);}


    /*==================================================================
    [ Play video 01 ]*/
    try {
        var srcOld = $('.video-mo-01').children('iframe').attr('src');

        $('[data-target="#modal-video-01"]').on('click',function(){
            $('.video-mo-01').children('iframe')[0].src += "&autoplay=1";

            setTimeout(function(){
                $('.video-mo-01').css('opacity','1');
            },300);
        });

        $('[data-dismiss="modal"]').on('click',function(){
            $('.video-mo-01').children('iframe')[0].src = srcOld;
            $('.video-mo-01').css('opacity','0');
        });
    } catch(er) {console.log(er);}


    /*==================================================================
    [ Tab mega menu ]*/
    try {
        $(window).on('load', function(){
            $('.sub-mega-menu .nav-pills > a').hover(function() {
                $(this).tab('show');
            });
        });
    } catch(er) {console.log(er);}

    /*==================================================================
    [ Slide100 txt ]*/

    try {
        $('.slide100-txt').each(function(){
            var slideTxt = $(this);
            var itemSlideTxt = $(this).find('.slide100-txt-item');
            var data = [];
            var count = 0;
            var animIn = $(this).data('in');
            var animOut = $(this).data('out');

            for(var i=0; i<itemSlideTxt.length; i++) {
                data[i] = $(itemSlideTxt[i]).clone();
                $(data[i]).addClass('clone');
            }

            $(window).on('load', function(){
                $(slideTxt).find('.slide100-txt-item').remove();
                $(slideTxt).append($(data[0]).clone());
                $(slideTxt).find('.slide100-txt-item.clone').addClass(animIn + ' visible-true');
                count = 0;
            });

            setInterval(function(){
                $(slideTxt).find('.slide100-txt-item.ab-t-l.' + animOut).remove();
                $(slideTxt).find('.slide100-txt-item').addClass('ab-t-l ' + animOut);


                if(count >= data.length-1) {
                    count = 0;
                }
                else {
                    count++;
                }

                // console.log($(data[count]).text());

                $(slideTxt).append($(data[count]).clone());
                $(slideTxt).find('.slide100-txt-item.clone').addClass(animIn + ' visible-true');
            },5000);
        });
    } catch(er) {console.log(er);}

    /*==================================================================
    [ Main Slider ]*/
    $('.mainSlider').slick({
        dots: true,
        autoplay: true,
        autoplaySpeed: 2000,
        infinite: true,
        speed: 500,
        fade: true,
        nav: false,
        cssEase: 'linear',
        rtl: true
    });


    $('.trendsSlider').slick({
        dots: true,
        autoplay: true,
        nav: false,
        autoplaySpeed: 2000,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        rtl: true
    });

    $('.relatedArticle_slider').slick({
        dots: false,
        nav: false,
        autoplay: true,
        autoplaySpeed: 2000,
        infinite: true,
        pauseOnHover: true,
        speed: 300,
        cssEase: 'linear',
        slidesToShow: 2,
        slidesToScroll: 1,
        rtl: true,
    });


    var swiper = new Swiper('#newsLeftBar_slider', {
        direction: 'vertical',
        slidesPerView: 4,
        spaceBetween: 0,
        loop: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: true,
        },
    });

    $(".swiper-container-vertical").mouseenter(function(){
        swiper.autoplay.stop();
    });

    $(".swiper-container-vertical").mouseleave(function(){
        swiper.autoplay.start();
    });



    $("#share").jsSocials({
        showCount: false,
        showLabel: true,
        shareIn: "popup",
        shares: [
            {share: "facebook", label: "شارك"},
            {share: "twitter", label: "تغريد"},
            {share: "linkedin", label: "شارك"}
        ],
    });

})(jQuery);


// Weather API
const weatherUrl = 'http://api.openweathermap.org/data/2.5/weather?id=360630&appid=29cbb1612d533e657bcb541c126d0fbd&units=metric';

$.get(weatherUrl, function(weather) {
    var temp_max = Number.parseFloat(weather.main.temp_max).toFixed(1);
    var temp_min = Number.parseFloat(weather.main.temp_min).toFixed(1);
    var humidity = Number.parseFloat(weather.main.humidity).toFixed(1);

    var sunrise = moment(weather.sys.sunrise * 1000).format('LT');
    var sunset = moment(weather.sys.sunset * 1000).format('LT');

    $('.weather').append(`
        <h2 class="text-center mb-2 mt-3">القاهره</h2>
        <div class="d-flex justify-content-around pt-2 pb-2 mb-3">
            <div class="morning">
                <i class="fas fa-cloud-sun"></i>
                <h4 dir="ltr">${temp_max} <sub>C</sub></h4>
            </div>
            <div class="night">
                <i class="fas fa-cloud-moon"></i>
                <h4 dir="ltr">${temp_min} <sub>C</sub></h4>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <div class="border-left p-2">
                <p>رطوبة</p>
                <p dir="ltr"> ${humidity} %</p>
            </div>
            <div class="border-left p-2">
                <p>الشروق</p>
                <p dir="ltr"> ${sunrise}</p>
            </div>
            <div class="p-2">
                <p>الغروب</p>
                <p dir="ltr"> ${sunset}</p>
            </div>
        </div>
    `);

});


//--------- Pray API
var toDay = moment().format('YYYY-MM-DD');
console.log(toDay);
const prayURL = `https://api.pray.zone/v2/times/day.json?city=cairo&date=${toDay}`;

var prayingName = {
    'Asr': 'العصر',
    'Dhuhr': 'الظهر',
    'Fajr': 'الفجر',
    'Imsak': 'الامساك',
    'Isha': 'العشاء',
    'Maghrib': 'المغرب',
    'Midnight': 'قيام الليل',
    'Sunrise': 'الضحي',
    'Sunset': 'الصبح'
};

$.get(prayURL,
    function(res) {
        var date = res.results.datetime[0].times;
        var p_name;
        $('.prayingTime').append(`<h4 class=text-center">يوم <br> ${toDay}</h4>`);
        for (const key in date) {
            const ele = date[key];
            p_name = prayingName[key];
            $('.prayingTime').append(`
                <div class="d-flex justify-content-around align-items-center">
                    <span class="name">${p_name}</span>
                    <span class="sdv-color">${ele}</span>
                </div>
            `);
        }
    }
);



// Currency API
const currencyUrl = 'https://v6.exchangerate-api.com/v6/e5fce7773d8d5d36c585ae74/latest/USD';
$.get(currencyUrl,
    function(res) {
        console.log(res);
        const d = new Date(res.time_last_update_utc);
        const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
        const mo = new Intl.DateTimeFormat('en', { month: 'short' }).format(d);
        const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);
        $('.currency').append(`

            <div class="text-center" style="padding: 1.5rem 0;">
                <p>سعر الدولار اليوم</p>
                <h2 style="font-size: 1.7rem;">${res.conversion_rates.EGP} جنيه</h2>
                <span style="direction: ltr;">${da}-${mo}-${ye}</span>
            </div>
        `);
    }
);