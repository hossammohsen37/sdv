$(document).ready(function (){
    // console.log('assign event to editor');

    // CKEDITOR.instances.content.on('change',function(){
    //     console.log('hello');
    // });
    var editor = CKEDITOR.instances.content;
    $("#analyzeButton").click(function(){
        analysis();
    });


    $('.__analysis').on('change', function() {
        setTimeout(() => {
            analysis();
        }, 3000);
    });

    CKEDITOR.instances.content.on('change', function() {
        setTimeout(() => {
            analysis();
        }, 3000);
    });


    function analysis() {
        var focusKeyword = $('#focus_keyword').val();
        var description = $('#description').val();
        var title = $('#title').val();
        // var content = editor.getData().replace(/(<([^>]+)>)/ig,"");
        var content = editor.getData();

        var seoAttributes = {
            keyword : focusKeyword,
            title : title,
            description : description
        };

        var readabilityResultsList = $('#readability-results-list');
        var seoResultsList = $('#seo-results-list');
        readabilityResultsList.html('');
        seoResultsList.html('');

        var readabilityDot = $('#readability-dot');
        var seoDot = $('#seo-dot');

        var analysisPromise = analyze(content,seoAttributes);
        console.log(analysisPromise);
        analysisPromise.then( ( results ) => {
            console.log( 'Analysis results:' );
            console.log( results );
            results.result.readability.results.forEach(result => {
                console.log('result');
                readabilityResultsList.append('<li>'+result.text+'</li>');
            });
            var readabilityScore = results.result.readability.score;

            var seoResults = results.result.seo[Object.keys(results.result.seo)[0]];
            console.log(seoResults);

            var seoScore = seoResults.score;

            seoResults.results.forEach(result => {
                console.log('result');
                seoResultsList.append('<li>'+result.text+'</li>');
            });

            renderScore(readabilityScore,readabilityDot);
            renderScore(seoScore,seoDot);
        });
    }
});



function renderScore(score, element)
{
    if(score > 40 && score < 70)
    {
        element.attr('class','align-middle seo-dot yellow');
    }
    else if(score > 70)
    {
        element.attr('class','align-middle seo-dot green');
    }
    else
    {
        element.attr('class','align-middle seo-dot red');
    }
}

async function analyze(content , attributes)
{
    // var results =  window.yoastAnalyzer.analyzeContent(content , attributes).then((results)=>{
    //     console.log('results is' + results);
    // });

    var results = await window.yoastAnalyzer.analyzeContent(content , attributes);
    // results.then( ( results ) => {
    //     console.log( 'Analysis results:' );
    //     console.log( results );
    //     return results;
    // } )
    console.log(results);
    return results;
}

