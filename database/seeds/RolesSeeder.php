<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
        [
            [    
                'name' => 'Super Admin',
                'name_ar' => 'سوبر ادمن',
                'guard_name' => 'web'
            ],
            [    
                'name' => 'Moderator',
                'name_ar' => 'مشرف',
                'guard_name' => 'web'
            ],
            [    
                'name' => 'Author',
                'name_ar' => 'كاتب',
                'guard_name' => 'web'
            ],
        ]);
    }
}
