<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Super Admin',
            'email' => 'admin@sdv.com',
            'password' => bcrypt('123456'),
            'level' => 'Super Admin',
        ]);
        $user->assignRole('Super Admin');
    }
}
