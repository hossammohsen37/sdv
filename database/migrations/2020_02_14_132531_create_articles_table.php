<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration {

	public function up()
	{
		Schema::create('articles', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title', 500)->nullable();
			$table->longText('content')->nullable();
			$table->bigInteger('author_id')->unsigned()->nullable();
			$table->bigInteger('category_id')->unsigned();
			$table->string('author_name', 255)->nullable();
			$table->boolean('verified')->default(false);
			$table->bigInteger('verified_by')->unsigned()->nullable();
			$table->timestamp('verified_at')->nullable();
			$table->bigInteger('visits')->default('0');
			$table->boolean('is_important')->nullable();
			$table->boolean('in_slider')->nullable();
			$table->boolean('is_news')->nullable();
			$table->boolean('is_infographic')->nullable();
			$table->integer('cover_type')->default(0)->comment('0:image,1:video,2:slider');
			$table->string('slug')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('articles');
	}
}