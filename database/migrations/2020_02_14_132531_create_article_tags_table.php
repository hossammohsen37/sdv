<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticleTagsTable extends Migration {

	public function up()
	{
		Schema::create('article_tags', function(Blueprint $table) {
			$table->increments('id');
			$table->bigInteger('article_id')->unsigned();
			$table->bigInteger('tag_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('article_tags');
	}
}