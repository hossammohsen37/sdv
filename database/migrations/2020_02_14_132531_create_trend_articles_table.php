<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrendArticlesTable extends Migration {

	public function up()
	{
		Schema::create('trend_articles', function(Blueprint $table) {
			$table->increments('id');
			$table->bigInteger('trend_id')->unsigned();
			$table->bigInteger('article_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('trend_articles');
	}
}