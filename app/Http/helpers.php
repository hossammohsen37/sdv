<?php

use Spatie\Permission\Models\Role;

/**
 * @return RolesCollection for the roles to be shown in forms
 */
function getRoles()
{
    return Role::all();
}