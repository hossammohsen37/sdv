<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use App\Models\Subscribtion;
use App\Models\Trend;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends BaseController
{
    public function index()
    {
        $importantCategories = Category::important()->get();
        $importantArticles   = Article::site()->important()->latest()->limit(4)->get();
        $homeCategories      = Category::home()->orderBy('homepage_order','asc')->get();
        $sliders             = Article::where('in_slider',1)->get();
        // $trends              = Trend::all();
        return view('website.home',compact('importantArticles','importantCategories','homeCategories','sliders'));
    }
    
    public function profile(Request $request, $id)
    {
        $author = User::find($id);
        $articles = Article::where('author_id',$id)->get();
        return view('website.author.author',compact('author','articles'));
    }

    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|email'
        ]);

        if($validator->fails())
        {
            return back()->withErrors($validator->errors());
        }

        Subscribtion::create($request->all());

        return back()->withSuccess('شكرا لاشتراكك معنا');
    }
}
