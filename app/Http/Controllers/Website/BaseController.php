<?php

namespace App\Http\Controllers\Website;

use App\Models\User;
use App\Models\Trend;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Sarfraznawaz2005\VisitLog\Facades\VisitLog;

class BaseController extends Controller
{
    public function __construct()
    {
        $infographic  = Article::where('is_infographic',1)->get()->last();
        $maxArticle   = Article::orderBy('visits','desc')->take(3)->get();
        $categories   = Category::parents()->get();
        $authorArticles  = Article::isAuthor()->get();
        $trends       = Trend::HasArticles()->get();
        $news         = Article::where('is_news',1)->get();
        VisitLog::save();
        view()->share('infographic',$infographic);
        view()->share('maxArticle',$maxArticle);
        view()->share('categories',$categories);
        view()->share('authorArticles',$authorArticles);
        view()->share('trends',$trends);
        view()->share('news',$news);
        
    }
}
