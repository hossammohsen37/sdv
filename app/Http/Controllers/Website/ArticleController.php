<?php

namespace App\Http\Controllers\Website;

use App\Models\Trend;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Filters\ArticleFilter;
use App\Http\Controllers\Controller;

class ArticleController extends BaseController
{
    public function index(Request $request, $categorySlug)
    { 
        $category = Category::where('name',$categorySlug)->first();
        $articles = $category->siteArticles()->latest()->paginate(12);
        $title    = $category->name;
        return view('website.article.grid-index',compact('articles','title'));   
    }

    public function mostImportant(Request $request,$categorySlug = null)
    {
        if($categorySlug)
        {
            $category = Category::where('name',$categorySlug)->first();
            $articles = $category->getImporantArticles()->paginate(12);
            $title = $category->title;
        }
        else
        {
            $articles = Article::site()->important()->paginate(12);
            $title = 'أهم الاحداث';
        }

        return view('website.article.grid-index',compact('articles','title'));   
    }

    public function single(Request $request, $articleSlug)
    {   
        $article = Article::where('slug',$articleSlug)->with('category')->first();
        $article->increment('visits');
        $similiarArticles = $article->similiarArticles();
        return view('website.article.single',compact('article','similiarArticles'));   
    }

    public function trendIndex($trendSlug)
    {
        $trend = Trend::where('slug',$trendSlug)->orWhere('id',$trendSlug)->first();
        $articles = $trend->articles()->paginate(12);
        $title = $trend->name;
        return view('website.article.grid-index',compact('title','articles'));
    }

    public function search(Request $request)
    {
        $filter = new ArticleFilter($request);
        $articles = Article::site()->filter($filter)->paginate(12);
        $title = $request->q;

        return view('website.article.grid-index',compact('articles','title'));
    }
}
