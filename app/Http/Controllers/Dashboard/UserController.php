<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:Super Admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        return view('dashboard.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = getRoles();
        $user = new User();
        return view('dashboard.users.form',compact('roles','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $requestData = $request->all();
        $requestData['password'] = Hash::make($requestData['password']);
        $requestData['level'] = Role::findById($request->role_id)->name;

        $user = User::create($requestData);
        $user->assignRole($request->role_id);

        if($request->hasFile('profile_pic'))
        {
            $user->updateProfilePic($request->profile_pic);
        }
        return redirect()->route('dashboard.users.index')->withSuccess('تم اضافة مستخدم بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = getRoles();
        return view('dashboard.users.form',compact('user','roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $requestData = $request->all();
        if(isset($requestData['password']))
        {
            $requestData['password'] = Hash::make($requestData['password']);
        }
        else
        {
            $requestData = $request->except('password');
        }
        $user->update($requestData);
        
        if($request->role_id)
            $user->syncRoles($request->role_id);

        if($request->hasFile('profile_pic'))
        {
            $user->updateProfilePic($request->profile_pic);
        }
        return redirect()->route('dashboard.users.index')->withSuccess('تم تعديل البيانات بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if($user->hasRole('Super Admin'))
        {
            $user->update(['delete_requested'=>true]);
            return redirect()->route('dashboard.users.index')->withSuccess('تم حذف المستخدم بنجاح وارسال رسالة الحذف له');
        }
        else
        {
            $user->update(['deleted_by'=>Auth::user()->id]);
            $user->delete();
            return redirect()->route('dashboard.users.index')->withSuccess('تم حذف المستخدم بنجاح');
        }
    }

    public function confirmDelete(Request $request)
    {
        $user = Auth::user();
        if($request->value == true)
        {
            $user->delete();
            return 'true';
        }
        else
        {
            $user->update(['delete_requested'=>false]);
            return 'false';
        }
    }
}
