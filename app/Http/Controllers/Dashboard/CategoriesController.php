<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:Super Admin']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(20);
        return view('dashboard.categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parentCategories = Category::parents()->get();
        $category = new Category();
        return view('dashboard.categories.form',compact('parentCategories','category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $category = Category::create($request->all());
        $category->refreshSlug();
        return redirect()->route('dashboard.categories.index')->withSuccess('Success','تمت اضافة القسم بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $parentCategories = Category::parents()->get();
        return view('dashboard.categories.form',compact('category','parentCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request,Category $category)
    {
        $category->update($request->all());
        $category->refreshSlug();
        return back()->withSuccess('تم تعديل القسم بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category  $cateogry
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return back()->withSuccess('تم الحذف بنجاح');
    }

    public function getChildren(Category $category)
    {
        if($category->children()->count())
        {
            $children = $category->children;
            return response()->json($children);
        }
        else
        {
            return 'false';
        }
    }

    public function orderIndex()
    {
        $homeCategories = Category::where('in_homepage',1)->orderBy('homepage_order','ASC')->get();
        $notHomeCategories = Category::where('in_homepage',0)->get();
        return view('dashboard.categories.order-categories',compact('homeCategories','notHomeCategories'));
    }

    public function orderSubmit(Request $request)
    {
        $sortingArr = $request->sortingArr;
        Category::query()->update([
            "in_homepage" => 0
        ]);
        foreach ($sortingArr as $index => $category_id) 
        {
            $category = Category::find($category_id);

            $category->update([
                "in_homepage"=>'1',
                "homepage_order"=>$index
            ]);    
        }

        return "success";
    }
}
