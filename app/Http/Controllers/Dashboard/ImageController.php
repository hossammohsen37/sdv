<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    public function editorUpload(Request $request)
    {
        $file = $request->upload->store('/public');
        $fileUrl = Storage::url($file);
        $fileName = basename($file);
        $response = [
            'fileName' => $fileName,
            'uploaded' => 1,
            'url' => $fileUrl,
        ];
        return response()->json($response);
    }
}
