<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Models\Author;
use App\Models\Category;
use App\Models\Trend;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::paginate(10);
        $trends = Trend::all();
        return view('dashboard.article.index',compact('articles','trends'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article    = new Article();
        $categories = Category::parents()->get();
        $authors    = Author::all();
        $img = null;
        return view('dashboard.article.form',compact('categories','authors','article','img'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {

        $requestData = $request->all();
        $article     = Article::create($requestData);
        $article->addMedia($request->article_image)
                ->toMediaCollection('article_coll');

        return redirect()->route('dashboard.article.index')->withSuccess('Success','تمت الاضافة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response 
     */
    public function show(Article $article)
    {
        return view('dashboard.article.single',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $categories = Category::parents()->get();
        $authors    = Author::all();
        $media      = $article->getMedia('article_coll')->first();
        $img        = $media->getFullUrl();
        return view('dashboard.article.form',compact('categories','authors','article','img'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $requestData = $request->all();
        $article->update($requestData);

        if($request->article_image != null)
        {
            $article->addMedia($request->article_image)
                    ->toMediaCollection('article_coll');
        }
        
        return redirect()->route('dashboard.article.index')->withSuccess('Success','تمت التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()->route('dashboard.article.index')->withSuccess('تم حذف المقال بنجاح');

    }

    public function saveArticle(Request $request,$article)
    {
        $article = Article::find($article);
        $article->update
            ([
                'is_news'=> $request->is_news, 
                'is_important'=> $request->is_important ,
                'is_slider'=> $request->is_slider ,
                'is_infographic'=> $request->is_infographic 
            ]);
            return back()->withSuccess('تم التعديل بنجاح');
    }
    
}
