<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Sarfraznawaz2005\VisitLog\Models\VisitLog;

class HomeController extends Controller
{
    /**
     * Return Dashboard Homepage
     */
    public function home(Request $request)
    {
        $articlesCount = Article::count();
        $unVerifiedAriclesCount = Article::where('verified',false)->count();
        $topTenArticles = Article::orderBy('visits','desc')->take(10)->get(); 
        $lastMonthVisits = VisitLog::where('created_at','>=',Carbon::now()->subMonth()->firstOfMonth())->count();
        $monthVisits = VisitLog::where('created_at','>=',Carbon::now()->firstOfMonth())->count();
        $chartDaysVisits = $this->getDaysVisitsArray(7);
        // dd($chartDaysVisits); 
        return view('dashboard.home',compact(
            'articlesCount',
            'unVerifiedAriclesCount',
            'lastMonthVisits',
            'monthVisits',
            'chartDaysVisits',
            'topTenArticles'
        ));
    }

    public function getDaysVisitsArray($daysNum)
    {
        $date = Carbon::now()->subDays($daysNum);

        $days = array();
        $daysLabels = array();
        $visits = array();
        for($i=0 ; $i<$daysNum+1 ; $i++)
        {
            array_push($daysLabels,$date->format('M-d'));
            // $dayVisitors = VisitLog::whereBetween('created_at',[$date->toDateTimeString(),$date->addDay()->toDayDateTimeString()])->count();
            $dayVisitors = VisitLog::whereDate('created_at',$date)->count();
            $date->addDay();
            array_push($days,$i+1);
            array_push($visits,$dayVisitors);
        }
        $days = json_encode($days);
        // dd(Carbon::now()->toDateTimeString());
        // dd(VisitLog::whereDate('created_at',Carbon::now())->count());
        $visits = json_encode($visits);
        // $visitsCount = VisitLog::count();
        $daysLabels = json_encode($daysLabels);

        return [
            'days' => $daysLabels,
            'visits' => $visits
        ];
    }
}
