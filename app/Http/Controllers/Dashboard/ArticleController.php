<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Models\ArticleTag;
use App\Models\Author;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Trend;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class ArticleController extends Controller
{
    public function __construct()
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $articles = Article::user()->paginate(10);
        $trends = Trend::all();
        return view('dashboard.article.index',compact('articles','trends'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article    = new Article();
        $categories = Category::parents()->get();
        $authors    = User::all();
        $img = null;
        return view('dashboard.article.form',compact('categories','authors','article','img'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
        $requestData = $request->all();

        if(!Auth::user()->hasRole(User::AUTHOR_ROLE))
        {
            $requestData['verified']    = true;
            $requestData['verified_by'] = Auth::user()->id;
        }

        $requestData['created_by'] = Auth::user()->id;

        if(isset($requestData['sub_category_id']))
        {
            $requestData['category_id'] = $requestData['sub_category_id'];
        }
        $article   = Article::create($requestData);
        if($request->hasFile('article_image'))
        {
            $article->addMedia($request->article_image)
            ->toMediaCollection('article_coll');
        }

        $tags      = $request->tags;
        $splitTags = explode(',',$tags);
        foreach($splitTags as  $splitTag )
        {
            $tagsTable = Tag::where('name',$splitTag)->first();
            if($tagsTable == null)
            {
                $tagsTable        = new Tag();
                $tagsTable->name  = $splitTag;
                $tagsTable->save();
            }

            $tagsArticleTable             = new ArticleTag();
            $tagsArticleTable->article_id = $article->id;
            $tagsArticleTable->tag_id     = $tagsTable->id;
            $tagsArticleTable->save();
        }
        $article->refreshSlug();

        if ($article->save())
        {
            \SEO\Seo::save($article, route('article.single', $article->slug), [
                    'title' => $article->title,
                    'images' => [
                    $article->cover_photo
                ]
            ]);
        }
        return redirect()->route('dashboard.article.index')->withSuccess('Success','تمت الاضافة بنجاح');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        return view('dashboard.article.single',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $categories = Category::parents()->get();
        $authors    = User::all();
        $media      = $article->getMedia('article_coll')->first();
        $img        = $media ? $media->getFullUrl() : '';
        $tags       = ArticleTag::where('article_id',$article->id)->get();
        return view('dashboard.article.form',compact('categories','authors','article','img','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $requestData = $request->all();

        if(isset($requestData['sub_category_id']))
        {
            $requestData['category_id'] = $requestData['sub_category_id'];
        }

        $article->update($requestData);

        if($request->article_image != null)
        {
            $article->addMedia($request->article_image)
                    ->toMediaCollection('article_coll');
        }
        $article->refreshSlug();

        if ($article->save())
        {
            \SEO\Seo::save($article, route('article.single', $article->slug), [
                    'title' => $article->title,
                    'images' => [
                    $article->cover_photo
                ]
            ]);
       }
        return redirect()->route('dashboard.article.index')->withSuccess('Success','تمت التعديل بنجاح');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return redirect()->route('dashboard.article.index')->withSuccess('تم حذف المقال بنجاح');
    }

    public function saveArticle(Request $request,$article)
    {
        $article = Article::find($article);
        $article->update
            ([
                'is_news'=> $request->is_news,
                'is_important'=> $request->is_important ,
                'in_slider'=> $request->is_slider ,
                'is_infographic'=> $request->is_infographic,
                'is_author' => $request->is_author
            ]);
        return back()->withSuccess('تم التعديل بنجاح');
    }

    public function addToTrend(Request $request)
    {
        $trend = Trend::find($request->trend_id);
        $articles = $request->articles;
        $trend->articles()->attach($articles);
        return response()->json(['message'=>'تم اضافة المقالات الى الترند بنجاح']);
    }

    public function verify(Article $article)
    {
        $article->update([
            'verified'=>true,
            'verified_by'=> Auth::user()->id
        ]);
        return back()->withSuccess('تم الموافقة على المقالة بنجاح');
    }

}
