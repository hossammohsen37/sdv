<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\TrendRequest;
use App\Models\Article;
use App\Models\Trend;
use Illuminate\Http\Request;

class TrendController extends Controller
{
    public function __construct()
    {
        $this->middleware(['role:Super Admin,Moderator']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trends = Trend::paginate(20);
        return view('dashboard.trend.index',compact('trends'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trend = new Trend();
        return view('dashboard.trend.form',compact('trend'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrendRequest $request)
    {
        $trend = Trend::create($request->all());
        if($request->hasFile('image'))
        {
            $trend->updateCoverImage($request->image);
        }
        $trend->refreshSlug();
        return redirect()->route('dashboard.trend.index')->withSuccess('تم اضافة التريند بنجاح');        
    }

    /**
     * Display the specified resource.
     *
     * @param  Trend  $trend
     * @return \Illuminate\Http\Response
     */
    public function show(Trend $trend)
    {
        $trendArticles = $trend->articles;
        return view('dashboard.trend.single',compact('trend','trendArticles'));   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Trend  $trend
     * @return \Illuminate\Http\Response
     */
    public function edit(Trend $trend)
    {
        return view('dashboard.trend.form',compact('trend'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Trend  $trend
     * @return \Illuminate\Http\Response
     */
    public function update(TrendRequest $request,Trend $trend)
    {
        $trend->update($request->all());
        if($request->hasFile('image'))
        {
            $trend->updateCoverImage($request->image);
        }
        $trend->refreshSlug();
        
        return redirect()->route('dashboard.trend.index')->withSuccess('تم تعديل التريند بنجاح');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Trend  $trend
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trend $trend)
    {
        $trend->delete();
        return back()->withSuccess('تم الحذف بنجاح');
    }

    public function removeArticle(Request $request,Trend $trend, Article $article)
    {
        $trend->articles()->detach($article->id);
        return back()->withSuccess('تم حذف المقالة من التريند بنجاح');
    }

    public function removeArticles(Request $request)
    {
            
    }
}
