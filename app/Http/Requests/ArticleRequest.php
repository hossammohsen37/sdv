<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(method_field('POST'))
        return [
            'title' => 'required',
            'category_id' => 'required',
            'author_id' => 'exists:users,id',
            'content' => 'required',
        ];
        elseif(method_field('PUT'))
        return [
            'title' => 'required',
            'category_id' => 'required',
            'author_id' => 'exists:users,id',
            'content' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title' => 'يجب ادخال العنوان',
            'category_id' => 'يجب اختيار النوع',
            'author_id' => 'يجب ادخال اسم الكاتب',
            'content' => 'يجب ادخال الموضوع',
        ];
    }
}
