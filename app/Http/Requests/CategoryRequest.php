<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(method_field('POST'))
        return [
            'name' => 'required|unique:categories,name',
            'parent_id' => 'exists:categories,id'
        ];
        elseif(method_field('PUT'))
        return [
            'name' => 'required|unique:categories,name,except,'.$this->route('category'),
            'parent_id' => 'exists:categories,id'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'يجب ادخال الاسم',
            'name.unique' => 'اسم القسم موجود من قبل'
        ];
    }
}
