<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Author extends Model 
{

    protected $table = 'authors';
    public $timestamps = true;

    use SoftDeletes,HasMediaTrait;

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

}