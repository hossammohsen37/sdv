<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statics extends Model
{
    protected $primaryKey = 'key';
    public $fillable = ['key','value_en','value_ar','has_lang'];

    public static function get($key,$lang='en')
    {
        $static = self::find($key);
        $valueAttr = 'value_'.$lang;
        return $static ? $static->$valueAttr : null;
    }
    
    public static function getWithCurrentLang($key)
    {
        $lang = session('lang','en');
        $static = self::find($key);
        if($static)
        {
            if($static->has_lang==1)
            {
                $valueAttr = 'value_'.$lang; 
                return $static ? $static->$valueAttr:null;
            }
            
            else
                return $static ? $static->value_en:null;
        }
    }

    public static function updateStatic($key,$value_en,$value_ar,$hasLang=true)
    {
        $static = self::find($key);
        if(!$static)
        {
            $static = self::create([
                "key"=>$key,
                "value_en"=>$value_en,
                "value_ar"=>$value_ar,
                "has_lang"=>$hasLang
            ]);
        }
        else
            $static->update(['value_en'=>$value_en,"value_ar"=>$value_ar]);
    }
    
    public static function updateStaticFile($key,$value)
    {
        $static = self::find($key);
        if(!$static)
        {
            $static = self::create([
                "key"=>$key,
                "value_en"=>$value->store('public/statics'),
                "has_lang"=>false
            ]);
        }
        else
            $static->update(['value_en'=>$value->store('public/statics')]);
    }

    public static function updateStatics(array $attrs,$files = null)
    {
        foreach($attrs as $key=>$value)
        {
            if(!isset($attrs[$key]))
                continue;
            if(gettype($value) == 'object')
            {
                self::updateStaticFile($key,$value);
                continue;

            }
            else
            {
                if(str_contains($key,'_en'))
                {
                    $enKey = $key;
                    // dd($key);
                    $key = substr($key,0,strpos($key,'_'));
                    $arKey = $key.'_ar';
                    $value_en = $value;
                    $value_ar = $attrs[$arKey];
                    unset($attrs[$enKey],$attrs[$arKey]);
                    self::updateStatic($key,$value_en,$value_ar,1);
                    continue;
                } 
                elseif(str_contains($key,'_ar'))
                {
                    $arKey = $key;
                    $key = substr($key,0,strpos($key,'_'));
                    $enKey = $key.'_en';
                    $value_ar = $value;
                    $value_en = $attrs[$enKey];
                    unset($attrs[$enKey],$attrs[$arKey]);
                    self::updateStatic($key,$value_en,$value_ar,1);
                    continue;

                } 
                else
                {
                    self::updateStatic($key,$value,null,0);  
                    continue;
                  
                }
            }
                        
        }

    }

    public static function deleteSliderImage($imageId)
    {
        $serializedArr = self::get('slider_images');
        $imagesArray = unserialize($serializedArr);
        $imagePositionInArray = array_search($imageId,$imagesArray);
        unset($imagesArray[$imagePositionInArray]);
        $serializedArr = serialize($imagesArray);
        self::updateStatic('slider_images',$serializedArr,null,0);
    }

    public static function emtpyKey($key)
    {
        self::updateStatic($key,null,null,0);
        
    }


}
