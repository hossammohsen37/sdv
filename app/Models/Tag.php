<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model 
{

    protected $table = 'tags';
    public $timestamps = true;

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'article_tages', 'tag_id', 'article_id');
    }

    
}