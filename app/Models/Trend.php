<?php

namespace App\Models;

use App\Traits\MakeSlug;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Trend extends Model implements HasMedia
{
    use SoftDeletes,HasMediaTrait,MakeSlug;

    protected $table = 'trends';
    protected $fillable = ['name','slug'];
    const COVER_IMAGE_MEDIA_COLLECTION = 'cover';
    
    public function articles()
    {
        return $this->belongsToMany(Article::class,'trend_articles','trend_id','article_id');
    }

    public function updateCoverImage($image)
    {
        foreach($this->getMedia(self::COVER_IMAGE_MEDIA_COLLECTION) as $image)
        {
            $image->delete();
        }
        $this->addMedia($image)->toMediaCollection(self::COVER_IMAGE_MEDIA_COLLECTION);
    }

    public function getCoverImageUrlAttribute()
    {
        $coverImages = $this->getMedia(self::COVER_IMAGE_MEDIA_COLLECTION);

        $coverImageUrl = $coverImages->first()? $coverImages->first()->getFullUrl() : null;    
        return $coverImageUrl;
    }

    public function getUrl()
    {
        $articles = $this->articles();
        if($articles->count()>1)
        {
            return route('trend.index',$this->slug??$this->id);
        }
        else
        {
            return route('article.single',$articles->first()->slug);
        }
    }

    public function refreshSlug()
    {
        $slug = $this->make_slug($this->name).'-'.$this->id;
        $this->update([
            'slug' => $slug,
        ]);
    }

    public function scopeHasArticles($query)
    {
        return $query->whereHas('articles');
    }
}