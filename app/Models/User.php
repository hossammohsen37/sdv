<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements HasMedia
{
    use Notifiable,HasRoles,HasMediaTrait,SoftDeletes;

    const PROFILE_PIC_MEDIA_COLLECTION = 'profile_pic';

    const ADMIN_ROLE = 1;
    const MODERATOR_ROLE = 2;
    const AUTHOR_ROLE = 3;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $guard_name = 'web';
    protected $guarded = ['id'];
    protected $fillable = ['name','email','password','level','about','delete_requested','deleted_by'];

    public function updateProfilePic($pic)
    {
        foreach($this->getMedia(self::PROFILE_PIC_MEDIA_COLLECTION) as $image)
        {
            $image->delete();
        }
        $this->addMedia($pic)->toMediaCollection(self::PROFILE_PIC_MEDIA_COLLECTION);
    }

    public function getRoleNameAttribute()
    {
        return $this->roles()->first()->name_ar;
    }

    public function getProfilePicAttribute()
    {
        $profilePics = $this->getMedia(self::PROFILE_PIC_MEDIA_COLLECTION);
        if($profilePics->first() !=null )
        {
            $profilePicUrl = $profilePics->first()->getFullUrl();
        }
        else
        {
            $profilePicUrl = null;    
        }    
        return $profilePicUrl;
    }

    public function canUseSeo()
    {
        if($this->hasRole('Super Admin'))
        {
            return true;
        }
    }
}
