<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrendArticle extends Model 
{

    protected $table = 'trend_articles';
    public $timestamps = true;

}