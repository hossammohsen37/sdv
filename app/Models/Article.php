<?php

namespace App\Models;

use App\Traits\MakeSlug;
use App\Traits\hasFilter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Article extends Model implements HasMedia
{
    use HasMediaTrait,hasFilter;

    protected $table = 'articles';
    protected $fillable = ['title','focus_keyword','description','content','author_id','category_id','author_name','verified_by','slug','cover_type','is_infographic','is_news','is_important','in_slider','visits','verified','is_author','created_by'];

    use SoftDeletes,MakeSlug;

    public const COVER_IMAGE = 0;
    public const COVER_VIDEO = 1;
    public const COVER_SLIDER = 2;
    public const READING_SPEED = 200;


    //region functions
    public function getCoverPhotoAttribute()
    {
        $media    = $this->getMedia('article_coll')->first();
        $img      = $media != null ? $media->getFullUrl(): '';
        return $img;
    }

    public function getTagsStringAttribute()
    {
        $tagsArray = $this->tags()->pluck('name')->toArray();
        $tagsString = implode(',',$tagsArray);
        return $tagsString;
    }

    public function getDisplayAuthorNameAttribute()
    {
        return $this->author_name;
    }

    public function refreshSlug()
    {
        $slug = $this->make_slug($this->title).'-'.$this->id;
        $this->update([
            'slug' => $slug,
        ]);
    }

    /**
     * Checks if this belongs to a category
     * @return bool
     */
    public function isMyCategory($category_id)
    {
        if( $this->category_id == $category_id || $this->category->parent_id == $category_id )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function similiarArticles()
    {
        $similiarArticles = Article::whereHas('tags', function ($query) {
            $tagIds = $this->tags()->pluck('tags.id')->all();
            $query->whereIn('tags.id', $tagIds);
        })->where('id', '<>', $this->id)->get();

        return $similiarArticles;
    }

    public function getReadingTimeAttribute()
    {
        $floatMinutes = $this->mb_str_word_count(strip_tags($this->content)) / 200;
        return sprintf('%02d:%02d', (int) $floatMinutes, fmod($floatMinutes, 1) * 60);
    }

    private function mb_str_word_count($string, $format = 0, $charlist = '[]')
    {
        $string=trim($string);
        if(empty($string))
            $words = array();
        else
            $words = preg_split('~[^\p{L}\p{N}\']+~u',$string);
        switch ($format) {
            case 0:
                return count($words);
                break;
            case 1:
            case 2:
                return $words;
                break;
            default:
                return $words;
                break;
        }
    }
    //endregion

    //region Relations
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class,'article_tags');
    }

    public function author()
    {
        return $this->belongsTo(User::class,'author_id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class,'created_by')->withDefault();
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_user_table', 'user_id', 'role_id');
    }

    public function trends()
    {
        return $this->belongsToMany(Trend::class, 'article_trends', 'article_id', 'trend_id');
    }
    //endregion

    //region Scopes
    public function scopeSite($query)
    {
        return $query->where('verified',true);
    }

    public function scopeImportant($query)
    {
        return $query->where('is_important',true);
    }

    public function scopeIsAuthor($query)
    {
        return $query->where('is_author',true)->where('author_id','!=',null);
    }

    /**
     * Getting the articles depending on user level
     */
    public function scopeUser($query)
    {
        $user = Auth::user();
        if($user->hasRole(User::AUTHOR_ROLE))
            return $query->where('author_id',$user->id);
        else
            return $query;
    }
    //endregion

}
