<?php

namespace App\Models;

use App\Traits\MakeSlug;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Category extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait,MakeSlug;

    protected $guarded = ['id'];
    protected $table = 'categories';
    public $timestamps = true;

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function siteArticles()
    {
        return $this->articles()->where('verified',true);
    }

    public function parent()
    {
        return $this->belongsTo(self::class,'parent_id')->withDefault();
    }

    public function scopeParents($query)
    {
        return $query->where('parent_id',null);
    }

    public function children()
    {
        return $this->hasMany(self::class,'parent_id');
    }

    public function scopeHome($query)
    {
        return $query->where('in_homepage',true)->orderBy('homepage_order');
    }

    public function scopeImportant($query)
    {
        return $query->whereHas('articles',function($q){
            return $q->where('is_important',true);
        });
    }

    /**
     * @return QueryBuilder
     */
    public function getImportantArticles()
    {
        return $this->siteArticles()->where('is_important',true)->latest();
    }

    public function refreshSlug()
    {
        $slug = $this->make_slug($this->name).'-'.$this->id;
        $this->update([
            'slug' => $slug,
        ]);
    }
}