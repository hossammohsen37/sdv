<?php
namespace App\Traits;

trait MakeSlug
{
    public function make_slug($string, $separator = '-')
    {
        $string = trim($string);
        $string = mb_strtolower($string, 'UTF-8');
        $string = preg_replace("/[^a-z0-9_\-\sءاآؤئبپتثجچحخدذرزژسشصضطظعغفقكکگلمنوهةىي]/u", '', $string);
        $string = preg_replace("/[\s\-_]+/", ' ', $string);
        $string = preg_replace("/[\s_]/", $separator, $string);

        return $string;
    }

    // private function make_slug($string, $separator = '-')
    // {
    //     $string = trim($string);
    //     $string = mb_strtolower($string, 'UTF-8');

    //     // Remove multiple dashes or whitespaces or underscores
    //     $string = preg_replace("/[\s-_]+/", ' ', $string) ;
    //     // Convert whitespaces and underscore to the given separator
    //     $string = preg_replace("/[\s_]/", $separator, $string);

    //     return rawurldecode($string);
    // }
}