<?php

namespace App\Filters;


class ArticleFilter extends Filters
{
    protected $var_filters = ['q','category_id','tag'];

    public function q($title)
    {
        return $this->builder->where('title','LIKE',"{$title}%");
    }
    public function tag($tag)
    {
        return $this->builder->whereHas('tags',function($query) use($tag){
            return $query->where('name',$tag);
        });
    }

    public function category_id($category_id)
    {
        return $this->builder->where('category_id',$category_id);
    }

    public function order_by($order_by)
    {
        $order = $this->request->order??'asc';
        return $this->builder->orderBy($order_by,$order);
    }
}
